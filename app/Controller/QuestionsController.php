<?php
App::uses('AppController', 'Controller');

class QuestionsController extends AppController {

	public $components = array('Paginator', 'Session');

	public function admin_index() {
		$this->Question->recursive = 0;
		$this->set('questions', $this->Paginator->paginate());
	}

	public function admin_view($id = null) {
		if (!$this->Question->exists($id)) {
			throw new NotFoundException(__('Invalid question'));
		}
		$options = array('conditions' => array('Question.' . $this->Question->primaryKey => $id));
		$this->set('question', $this->Question->find('first', $options));
	}

	public function admin_add($exam_id = null) {
        if(!empty($exam_id)) {
            if (!$this->Question->OnlineExam->exists($exam_id)) {
                throw new NotFoundException(__('Invalid Exam'));
            }
            $this->set(compact('exam_id'));
        }
		if ($this->request->is('post')) {
			$this->Question->create();
			if ($this->Question->save($this->request->data)) {
				$this->Session->setFlash(__('The question has been saved.'));
                if(!empty($exam_id)) {
                    return $this->redirect(array('controller'=>'online_exams', 'action' => 'questions', $exam_id));
                }
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The question could not be saved. Please, try again.'));
			}
		}
		$subjects = $this->Question->Subject->find('list');
		$admissionTypes = $this->Question->AdmissionType->find('list');
		$universities = $this->Question->University->find('list');
		$onlineExams = $this->Question->OnlineExam->find('list');
		$lectures = $this->Question->Lecture->find('list');
		$this->set(compact('subjects', 'admissionTypes', 'universities', 'onlineExams', 'lectures'));
	}

    public function admin_upload_csv($exam_id=null) {
        /*set_time_limit(0);
        if ($this->request->is('post')) {
            $i = 0;
            $this->loadModel('Breed');
            if (!empty($this->request->data['CmsUser']['filename']['tmp_name'])) {
                //Import uploaded file to Database
                $import_data['Breed'] = array();
                $column = array();
                $main_array = array();
                $handle = fopen($this->request->data['CmsUser']['filename']['tmp_name'], "r");
                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                    #AuthComponent::_setTrace($data);
                    if ($i > 0) {
                        foreach($data as $key => $item){
                            if($column[$key] == 'Weight') $item = str_ireplace('lbs', ' lbs', $item);
                            $temp = array(
                                $column[$key] => $item
                            );
                            $main_array = am($main_array, $temp);
                        }
                        $output = array('Breed' => $main_array);
                        #AuthComponent::_setTrace($output, false);
                        $this->Breed-> create();
                        $this->Breed->save($output);
                    } else{
                        $column = $data;
                    }
                    $i++;
                }

                fclose($handle);
                $this->Session->setFlash(__('The Spreadsheet has been saved.'));
            }
        }*/
    }

	public function admin_edit($id = null, $exam_id = null) {
		if (!$this->Question->exists($id)) {
			throw new NotFoundException(__('Invalid question'));
		}
        if(!empty($exam_id)) {
            if (!$this->Question->OnlineExam->exists($exam_id)) {
                throw new NotFoundException(__('Invalid Exam'));
            }
            $this->set(compact('exam_id'));
        }

		if ($this->request->is(array('post', 'put'))) {
			if ($this->Question->save($this->request->data)) {
				$this->Session->setFlash(__('The question has been saved.'));
                if(!empty($exam_id)) {
                    return $this->redirect(array('controller'=>'online_exams', 'action' => 'questions', $exam_id));
                }
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The question could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Question.' . $this->Question->primaryKey => $id));
			$this->request->data = $this->Question->find('first', $options);
		}
		$subjects = $this->Question->Subject->find('list');
		$admissionTypes = $this->Question->AdmissionType->find('list');
		$universities = $this->Question->University->find('list');
		$onlineExams = $this->Question->OnlineExam->find('list');
		$lectures = $this->Question->Lecture->find('list');
		$this->set(compact('subjects', 'admissionTypes', 'universities', 'onlineExams', 'lectures'));
	}

	public function admin_delete($id = null, $exam_id = null) {
		$this->Question->id = $id;
		if (!$this->Question->exists()) {
			throw new NotFoundException(__('Invalid question'));
		}
        if(!empty($exam_id)) {
            if (!$this->Question->OnlineExam->exists($exam_id)) {
                throw new NotFoundException(__('Invalid Exam'));
            }
        }

		$this->request->allowMethod('post', 'delete');
		if ($this->Question->delete()) {
			$this->Session->setFlash(__('The question has been deleted.'));
		} else {
			$this->Session->setFlash(__('The question could not be deleted. Please, try again.'));
		}

        if(!empty($exam_id)) {
            return $this->redirect(array('controller'=>'online_exams', 'action' => 'questions', $exam_id));
        }
		return $this->redirect(array('action' => 'index'));
	}

    public function admin_remove_from_exam($id = null, $exam_id = null) {
        $this->Question->id = $id;
        if (!$this->Question->exists()) {
            throw new NotFoundException(__('Invalid question'));
        }
        if (!$this->Question->OnlineExam->exists($exam_id)) {
            throw new NotFoundException(__('Invalid Exam'));
        }

        $this->request->allowMethod('post', 'delete');
        $data['Question']['online_exam_id'] = null;
        $this->Question->id = $id;
        if ($this->Question->save($data)) {
            $this->Session->setFlash(__('The question has been removed from this exam but remains in database.'));
        } else {
            $this->Session->setFlash(__('The question could not be removed. Please, try again.'));
        }

        if(!empty($exam_id)) {
            return $this->redirect(array('controller'=>'online_exams', 'action' => 'questions', $exam_id));
        }
        return $this->redirect(array('action' => 'index'));
    }
}
