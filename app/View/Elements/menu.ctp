<?php
    $controller = $this->params['controller'];
    $action = $this->params['action'];
    //$method = $this->params['pass'][0];
?>
<div class="actions">
	<h3><?php echo __('Dashboard'); ?></h3>
	<ul>
		<li class="menu <?php if( $controller == 'online_exams' || $controller == 'online_exam_histories' ) echo "selected";?> ">
            <?php echo $this->Html->link(__('Online Exams'), array('controller' => 'online_exams', 'action' => 'index')); ?>
        </li>
        <?php if($controller == 'online_exams'){ ?>
            <ul style="margin-top: 10px;">
                <li class="sub-menu <?php if( $controller == 'online_exams' && $action=='admin_add') echo "selected2";?> ">
                    <?php echo $this->Html->link(__('Add'), array('controller' => 'online_exams', 'action' => 'add')); ?>
                </li>
            </ul>
        <?php } ?>

        <li class="menu <?php if( $controller == 'documents' ) echo "selected";?> ">
            <?php echo $this->Html->link(__('Documents'), array('controller' => 'documents', 'action' => 'index')); ?>
        </li>
        <?php if($controller == 'documents'){ ?>
            <ul style="margin-top: 10px;">
                <li class="sub-menu <?php if( $controller == 'documents' && $action=='admin_add') echo "selected2";?> ">
                    <?php echo $this->Html->link(__('Add'), array('controller' => 'documents', 'action' => 'add')); ?>
                </li>
            </ul>
        <?php } ?>

        <li><?php echo $this->Html->link(__('Logout'), array('controller' => 'cms_users', 'action' => 'logout')); ?></li>
	</ul>
</div>