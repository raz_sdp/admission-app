<div class="badgesUsers view">
<h2><?php echo __('Badges User'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($badgesUser['BadgesUser']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($badgesUser['User']['name'], array('controller' => 'users', 'action' => 'view', $badgesUser['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Badge'); ?></dt>
		<dd>
			<?php echo $this->Html->link($badgesUser['Badge']['name'], array('controller' => 'badges', 'action' => 'view', $badgesUser['Badge']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($badgesUser['BadgesUser']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($badgesUser['BadgesUser']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Badges User'), array('action' => 'edit', $badgesUser['BadgesUser']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Badges User'), array('action' => 'delete', $badgesUser['BadgesUser']['id']), array(), __('Are you sure you want to delete # %s?', $badgesUser['BadgesUser']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Badges Users'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Badges User'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Badges'), array('controller' => 'badges', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Badge'), array('controller' => 'badges', 'action' => 'add')); ?> </li>
	</ul>
</div>
