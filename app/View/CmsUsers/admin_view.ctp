<div class="cmsUsers view">
<h2><?php echo __('Cms User'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($cmsUser['CmsUser']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Username'); ?></dt>
		<dd>
			<?php echo h($cmsUser['CmsUser']['username']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email'); ?></dt>
		<dd>
			<?php echo h($cmsUser['CmsUser']['email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Password'); ?></dt>
		<dd>
			<?php echo h($cmsUser['CmsUser']['password']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Simple Pwd'); ?></dt>
		<dd>
			<?php echo h($cmsUser['CmsUser']['simple_pwd']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Role'); ?></dt>
		<dd>
			<?php echo $this->Html->link($cmsUser['Role']['name'], array('controller' => 'roles', 'action' => 'view', $cmsUser['Role']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($cmsUser['CmsUser']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($cmsUser['CmsUser']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Cms User'), array('action' => 'edit', $cmsUser['CmsUser']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Cms User'), array('action' => 'delete', $cmsUser['CmsUser']['id']), array(), __('Are you sure you want to delete # %s?', $cmsUser['CmsUser']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Cms Users'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cms User'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Roles'), array('controller' => 'roles', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Role'), array('controller' => 'roles', 'action' => 'add')); ?> </li>
	</ul>
</div>
