<div class="universities view">
<h2><?php echo __('University'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($university['University']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($university['University']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Website'); ?></dt>
		<dd>
			<?php echo h($university['University']['website']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Address'); ?></dt>
		<dd>
			<?php echo h($university['University']['address']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('City'); ?></dt>
		<dd>
			<?php echo h($university['University']['city']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($university['University']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($university['University']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($university['University']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit University'), array('action' => 'edit', $university['University']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete University'), array('action' => 'delete', $university['University']['id']), array(), __('Are you sure you want to delete # %s?', $university['University']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Universities'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New University'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Questions'), array('controller' => 'questions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Question'), array('controller' => 'questions', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Questions'); ?></h3>
	<?php if (!empty($university['Question'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Subject Id'); ?></th>
		<th><?php echo __('Admission Type Id'); ?></th>
		<th><?php echo __('Question'); ?></th>
		<th><?php echo __('Ans A'); ?></th>
		<th><?php echo __('Ans B'); ?></th>
		<th><?php echo __('Ans C'); ?></th>
		<th><?php echo __('Ans D'); ?></th>
		<th><?php echo __('Ans E'); ?></th>
		<th><?php echo __('Cor Ans'); ?></th>
		<th><?php echo __('Explanation'); ?></th>
		<th><?php echo __('University Id'); ?></th>
		<th><?php echo __('Year'); ?></th>
		<th><?php echo __('Online Exam Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($university['Question'] as $question): ?>
		<tr>
			<td><?php echo $question['id']; ?></td>
			<td><?php echo $question['subject_id']; ?></td>
			<td><?php echo $question['admission_type_id']; ?></td>
			<td><?php echo $question['question']; ?></td>
			<td><?php echo $question['ans_a']; ?></td>
			<td><?php echo $question['ans_b']; ?></td>
			<td><?php echo $question['ans_c']; ?></td>
			<td><?php echo $question['ans_d']; ?></td>
			<td><?php echo $question['ans_e']; ?></td>
			<td><?php echo $question['cor_ans']; ?></td>
			<td><?php echo $question['explanation']; ?></td>
			<td><?php echo $question['university_id']; ?></td>
			<td><?php echo $question['year']; ?></td>
			<td><?php echo $question['online_exam_id']; ?></td>
			<td><?php echo $question['created']; ?></td>
			<td><?php echo $question['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'questions', 'action' => 'view', $question['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'questions', 'action' => 'edit', $question['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'questions', 'action' => 'delete', $question['id']), array(), __('Are you sure you want to delete # %s?', $question['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Question'), array('controller' => 'questions', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
