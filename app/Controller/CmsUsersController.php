<?php
App::uses('AppController', 'Controller');

class CmsUsersController extends AppController {

	public $components = array('Paginator', 'Session');

	public function admin_index() {
		$this->CmsUser->recursive = 0;
		$this->set('cmsUsers', $this->Paginator->paginate());
	}

	public function admin_view($id = null) {
		if (!$this->CmsUser->exists($id)) {
			throw new NotFoundException(__('Invalid cms user'));
		}
		$options = array('conditions' => array('CmsUser.' . $this->CmsUser->primaryKey => $id));
		$this->set('cmsUser', $this->CmsUser->find('first', $options));
	}

	public function admin_add() {
		if ($this->request->is('post')) {
            if(!empty($this->request->data['CmsUser']['simple_pwd'])){
                $this->request->data['CmsUser']['simple_pwd'] = $this->request->data['CmsUser']['simple_pwd'];
                $this->request->data['CmsUser']['password'] = AuthComponent::password($this->request->data['CmsUser']['simple_pwd']);
            }
			$this->CmsUser->create();
			if ($this->CmsUser->save($this->request->data)) {
				$this->Session->setFlash(__('The cms user has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The cms user could not be saved. Please, try again.'));
			}
		}
		$roles = $this->CmsUser->Role->find('list');
		$this->set(compact('roles'));
	}

	public function admin_edit($id = null) {
		if (!$this->CmsUser->exists($id)) {
			throw new NotFoundException(__('Invalid cms user'));
		}
		if ($this->request->is(array('post', 'put'))) {
            if(!empty($this->request->data['CmsUser']['simple_pwd'])){
                $this->request->data['CmsUser']['simple_pwd'] = $this->request->data['CmsUser']['simple_pwd'];
                $this->request->data['CmsUser']['password'] = AuthComponent::password($this->request->data['CmsUser']['simple_pwd']);
            }
            $this->CmsUser->id = $id;
			if ($this->CmsUser->save($this->request->data)) {
				$this->Session->setFlash(__('The cms user has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The cms user could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('CmsUser.' . $this->CmsUser->primaryKey => $id));
			$this->request->data = $this->CmsUser->find('first', $options);
		}
		$roles = $this->CmsUser->Role->find('list');
		$this->set(compact('roles'));
	}

	public function admin_delete($id = null) {
		$this->CmsUser->id = $id;
		if (!$this->CmsUser->exists()) {
			throw new NotFoundException(__('Invalid cms user'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->CmsUser->delete()) {
			$this->Session->setFlash(__('The cms user has been deleted.'));
		} else {
			$this->Session->setFlash(__('The cms user could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

    public function admin_login(){
        if($this->request->is('post')){
            $password = AuthComponent::password($this->request->data['CmsUser']['password']);
            #die($password);
            $query = array(
                'conditions' => array(
                    'CmsUser.username' => $this->request->data['CmsUser']['username'],
                    'CmsUser.password' => $password,
                ),
            );
            $is_exist = $this->CmsUser->find('first', $query);
            if(!empty($is_exist)){
                $this->Auth->login($this->request->data['CmsUser']);
                return $this->redirect($this->Auth->redirect());
            } else {
                $this->Auth->logout();
                $this->Session->setFlash(__('Incorrect username or password, please try again.'), 'default', array('class' => 'error'));
            }
        }
    }

    public function admin_dashboard(){

    }

    public function admin_logout() {
        return $this->redirect($this->Auth->logout());
    }
}
