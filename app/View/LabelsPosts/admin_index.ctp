<div class="labelsPosts index">
	<h2><?php echo __('Labels Posts'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('post_id'); ?></th>
			<th><?php echo $this->Paginator->sort('label_id'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($labelsPosts as $labelsPost): ?>
	<tr>
		<td><?php echo h($labelsPost['LabelsPost']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($labelsPost['Post']['title'], array('controller' => 'posts', 'action' => 'view', $labelsPost['Post']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($labelsPost['Label']['name'], array('controller' => 'labels', 'action' => 'view', $labelsPost['Label']['id'])); ?>
		</td>
		<td><?php echo h($labelsPost['LabelsPost']['created']); ?>&nbsp;</td>
		<td><?php echo h($labelsPost['LabelsPost']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $labelsPost['LabelsPost']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $labelsPost['LabelsPost']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $labelsPost['LabelsPost']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $labelsPost['LabelsPost']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Labels Post'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Posts'), array('controller' => 'posts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Post'), array('controller' => 'posts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Labels'), array('controller' => 'labels', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Label'), array('controller' => 'labels', 'action' => 'add')); ?> </li>
	</ul>
</div>
