<?php
App::uses('AppController', 'Controller');

class OnlineExamsController extends AppController {

	public $components = array('Paginator', 'Session');

    public function beforeFilter(){
        $this->Auth->allow('get_questions', 'get_exam_list', 'get_exam_result');
    }

	public function admin_index() {
		$this->OnlineExam->recursive = 0;
        $this->paginate = array('all',
            'order' => array('OnlineExam.starting_time')
        );
		$this->set('onlineExams', $this->Paginator->paginate());
	}

	public function admin_view($id = null) {
		if (!$this->OnlineExam->exists($id)) {
			throw new NotFoundException(__('Invalid online exam'));
		}
		$options = array('conditions' => array('OnlineExam.' . $this->OnlineExam->primaryKey => $id));
		$this->set('onlineExam', $this->OnlineExam->find('first', $options));
	}

	public function admin_add() {
		if ($this->request->is('post')) {
			$this->OnlineExam->create();
			if ($this->OnlineExam->save($this->request->data)) {
				$this->Session->setFlash(__('The online exam has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The online exam could not be saved. Please, try again.'));
			}
		}
	}

	public function admin_edit($id = null) {
		if (!$this->OnlineExam->exists($id)) {
			throw new NotFoundException(__('Invalid online exam'));
		}
		if ($this->request->is(array('post', 'put'))) {
            $this->OnlineExam->id = $id;
			if ($this->OnlineExam->save($this->request->data)) {
				$this->Session->setFlash(__('The online exam has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The online exam could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('OnlineExam.' . $this->OnlineExam->primaryKey => $id));
			$this->request->data = $this->OnlineExam->find('first', $options);
		}
	}

	public function admin_delete($id = null) {
		$this->OnlineExam->id = $id;
		if (!$this->OnlineExam->exists()) {
			throw new NotFoundException(__('Invalid online exam'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->OnlineExam->delete()) {
			$this->Session->setFlash(__('The online exam has been deleted.'));
		} else {
			$this->Session->setFlash(__('The online exam could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

    public function admin_questions($id) {
        $this->OnlineExam->id = $id;
        if (!$this->OnlineExam->exists()) {
            throw new NotFoundException(__('Invalid online exam'));
        }
        $this->OnlineExam->recursive = 2;
        $online_exam = $this->OnlineExam->findById($id);
        $this->set(compact('online_exam'));
    }

    public function admin_results($id) {
        $this->OnlineExam->id = $id;
        if (!$this->OnlineExam->exists()) {
            throw new NotFoundException(__('Invalid online exam'));
        }
        $this->OnlineExam->recursive = 2;
        $online_exam = $this->OnlineExam->findById($id);
        $this->set(compact('online_exam'));
    }

    public function get_exam_list() {
        header('Content-Type: application/json');
        $this->autoLayout = false;
        $this->autRender = false;

        $this->OnlineExam->recursive = 0;
        $exams = $this->OnlineExam->find('all', array('conditions' => array('(OnlineExam.starting_at + INTERVAL OnlineExam.time MINUTE) > NOW()')));

        foreach($exams as $key => $item) {
            $rem_time = 0;
            $running = false;
            $time = new DateTime($item['OnlineExam']['starting_at']);
            $time->add(new DateInterval('PT' . $item['OnlineExam']['time'] . 'M'));
            $now = new DateTime();
            $time_start = new DateTime($item['OnlineExam']['starting_at']);
            if($now < $time && $time_start <= $now) {
                $running = true;
            } else {
                $rem_time = (array) $time_start->diff($now);
                foreach($rem_time as $k => $i) {
                    if(!($k=='m' || $k=='d' || $k=='h' || $k=='i' || $k=='s')) {
                        unset($rem_time[$k]);
                    }
                }
            }

            $exams[$key]['OnlineExam']['id'] = (int) $exams[$key]['OnlineExam']['id'];
            $exams[$key]['OnlineExam']['time'] = (int) $exams[$key]['OnlineExam']['time'];
            $exams[$key]['OnlineExam']['no_ques'] = (int) $exams[$key]['OnlineExam']['no_ques'];
            $exams[$key]['OnlineExam']['marks'] = (int) $exams[$key]['OnlineExam']['marks'];

            $exams[$key]['OnlineExam']['running'] = $running;
            $exams[$key]['OnlineExam']['remaining_time_to_start'] = $rem_time;
        }

        if(!empty($exams)) {
            die(json_encode(array('success'=>true, 'exams' => Hash::extract($exams, '{n}.OnlineExam'))));
        } else die(json_encode(array('success'=>false, 'msg' => 'Sorry! No exam available.')));
    }

    public function get_questions($id) {
        header('Content-Type: application/json');
        $this->autoLayout = false;
        $this->autRender = false;

        $this->OnlineExam->id = $id;
        if (!$this->OnlineExam->exists()) {
            die(json_encode(array('success'=>false, 'msg' => 'Sorry! Invalid exam.')));
        }

        $this->OnlineExam->recursive = 2;
        $online_exam = $this->OnlineExam->findById($id);

        $time = new DateTime($online_exam['OnlineExam']['starting_at']);
        $time->add(new DateInterval('PT' . $online_exam['OnlineExam']['time'] . 'M'));
        $now = new DateTime();
        if($time < $now) {
            die(json_encode(array('success'=>false, 'msg' => 'Sorry! Exam Over.')));
        }

        $time_start = new DateTime($online_exam['OnlineExam']['starting_at']);
        $running = false;
        $rem_time = 0;
        if($now < $time && $time_start <= $now) {
            $running = true;
        } else {
            $rem_time = (array) $time_start->diff($now);
            foreach($rem_time as $k => $i) {
                if(!($k=='m' || $k=='d' || $k=='h' || $k=='i' || $k=='s')) {
                    unset($rem_time[$k]);
                }
            }
        }

        $exam = $online_exam['OnlineExam'];
        $exam['running'] = $running;
        $exam['remaining_time_to_start'] = $rem_time;

        $exam['id'] = (int) $exam['id'];
        $exam['time'] = (int) $exam['time'];
        $exam['no_ques'] = (int) $exam['no_ques'];
        $exam['marks'] = (int) $exam['marks'];

        $questions = array();
        if($running) {
            foreach($online_exam['Question'] as $item) {
                $question['subject'] = $item['Subject']['name'];
                $question['question'] = empty($item['question']) ? null : $item['question'];
                $question['ans_a'] = empty($item['ans_a']) ? null : $item['ans_a'];
                $question['ans_b'] = empty($item['ans_b']) ? null : $item['ans_b'];
                $question['ans_c'] = empty($item['ans_c']) ? null : $item['ans_c'];
                $question['ans_d'] = empty($item['ans_d']) ? null : $item['ans_d'];
                $question['ans_e'] = empty($item['ans_e']) ? null : $item['ans_e'];
                $question['cor_ans'] = $item['cor_ans'];
                array_push($questions, $question);
            }
        }
        die(json_encode(array('success'=>true, 'exam'=>$exam, 'questions' => $questions)));
    }

    public function get_exam_result($id = null) {
        header('Content-Type: application/json');
        $this->autoLayout = false;
        $this->autRender = false;

        $this->OnlineExam->id = $id;
        if (!$this->OnlineExam->exists()) {
            die(json_encode(array('success'=>false, 'msg' => 'Sorry! Invalid exam.')));
        }

        $this->OnlineExam->recursive = 2;
        $online_exam = $this->OnlineExam->findById($id);
        $exam = $online_exam['OnlineExam'];
        $results = $online_exam['OnlineExamHistory'];
        foreach($results as $key=> $item) {
            $results[$key]['user'] = $results[$key]['User']['name'];
            unset(
                $results[$key]['id'],
                $results[$key]['online_exam_id'],
                $results[$key]['user_id'],
                $results[$key]['OnlineExam'],
                $results[$key]['User'],
                $results[$key]['created'],
                $results[$key]['modified']
            );
        }

        usort($results, function($a, $b) {
            return $b['mark'] - $a['mark'];
        });

        die(json_encode(array('success'=>true, 'exam'=>$exam, 'results' => $results)));
    }
}
