<div class="lectures view">
<h2><?php echo __('Lecture'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($lecture['Lecture']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Lesson'); ?></dt>
		<dd>
			<?php echo $this->Html->link($lecture['Lesson']['name'], array('controller' => 'lessons', 'action' => 'view', $lecture['Lesson']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($lecture['Lecture']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Lecture'); ?></dt>
		<dd>
			<?php echo h($lecture['Lecture']['lecture']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Order'); ?></dt>
		<dd>
			<?php echo h($lecture['Lecture']['order']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($lecture['Lecture']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($lecture['Lecture']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Lecture'), array('action' => 'edit', $lecture['Lecture']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Lecture'), array('action' => 'delete', $lecture['Lecture']['id']), array(), __('Are you sure you want to delete # %s?', $lecture['Lecture']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Lectures'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Lecture'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Lessons'), array('controller' => 'lessons', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Lesson'), array('controller' => 'lessons', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Completance Histories'), array('controller' => 'completance_histories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Completance History'), array('controller' => 'completance_histories', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Questions'), array('controller' => 'questions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Question'), array('controller' => 'questions', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Completance Histories'); ?></h3>
	<?php if (!empty($lecture['CompletanceHistory'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Lecture Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($lecture['CompletanceHistory'] as $completanceHistory): ?>
		<tr>
			<td><?php echo $completanceHistory['id']; ?></td>
			<td><?php echo $completanceHistory['user_id']; ?></td>
			<td><?php echo $completanceHistory['lecture_id']; ?></td>
			<td><?php echo $completanceHistory['created']; ?></td>
			<td><?php echo $completanceHistory['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'completance_histories', 'action' => 'view', $completanceHistory['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'completance_histories', 'action' => 'edit', $completanceHistory['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'completance_histories', 'action' => 'delete', $completanceHistory['id']), array(), __('Are you sure you want to delete # %s?', $completanceHistory['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Completance History'), array('controller' => 'completance_histories', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Questions'); ?></h3>
	<?php if (!empty($lecture['Question'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Subject Id'); ?></th>
		<th><?php echo __('Admission Type Id'); ?></th>
		<th><?php echo __('Question'); ?></th>
		<th><?php echo __('Ans A'); ?></th>
		<th><?php echo __('Ans B'); ?></th>
		<th><?php echo __('Ans C'); ?></th>
		<th><?php echo __('Ans D'); ?></th>
		<th><?php echo __('Ans E'); ?></th>
		<th><?php echo __('Cor Ans'); ?></th>
		<th><?php echo __('Explanation'); ?></th>
		<th><?php echo __('University Id'); ?></th>
		<th><?php echo __('Year'); ?></th>
		<th><?php echo __('Online Exam Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($lecture['Question'] as $question): ?>
		<tr>
			<td><?php echo $question['id']; ?></td>
			<td><?php echo $question['subject_id']; ?></td>
			<td><?php echo $question['admission_type_id']; ?></td>
			<td><?php echo $question['question']; ?></td>
			<td><?php echo $question['ans_a']; ?></td>
			<td><?php echo $question['ans_b']; ?></td>
			<td><?php echo $question['ans_c']; ?></td>
			<td><?php echo $question['ans_d']; ?></td>
			<td><?php echo $question['ans_e']; ?></td>
			<td><?php echo $question['cor_ans']; ?></td>
			<td><?php echo $question['explanation']; ?></td>
			<td><?php echo $question['university_id']; ?></td>
			<td><?php echo $question['year']; ?></td>
			<td><?php echo $question['online_exam_id']; ?></td>
			<td><?php echo $question['created']; ?></td>
			<td><?php echo $question['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'questions', 'action' => 'view', $question['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'questions', 'action' => 'edit', $question['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'questions', 'action' => 'delete', $question['id']), array(), __('Are you sure you want to delete # %s?', $question['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Question'), array('controller' => 'questions', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
