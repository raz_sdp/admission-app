<?php
App::uses('AppController', 'Controller');
/**
 * AdmissionTypes Controller
 *
 * @property AdmissionType $AdmissionType
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class AdmissionTypesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->AdmissionType->recursive = 0;
		$this->set('admissionTypes', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->AdmissionType->exists($id)) {
			throw new NotFoundException(__('Invalid admission type'));
		}
		$options = array('conditions' => array('AdmissionType.' . $this->AdmissionType->primaryKey => $id));
		$this->set('admissionType', $this->AdmissionType->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->AdmissionType->create();
			if ($this->AdmissionType->save($this->request->data)) {
				$this->Session->setFlash(__('The admission type has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The admission type could not be saved. Please, try again.'));
			}
		}
		$users = $this->AdmissionType->User->find('list');
		$this->set(compact('users'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->AdmissionType->exists($id)) {
			throw new NotFoundException(__('Invalid admission type'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->AdmissionType->save($this->request->data)) {
				$this->Session->setFlash(__('The admission type has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The admission type could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('AdmissionType.' . $this->AdmissionType->primaryKey => $id));
			$this->request->data = $this->AdmissionType->find('first', $options);
		}
		$users = $this->AdmissionType->User->find('list');
		$this->set(compact('users'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->AdmissionType->id = $id;
		if (!$this->AdmissionType->exists()) {
			throw new NotFoundException(__('Invalid admission type'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->AdmissionType->delete()) {
			$this->Session->setFlash(__('The admission type has been deleted.'));
		} else {
			$this->Session->setFlash(__('The admission type could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
