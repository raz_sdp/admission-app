<?php
App::uses('AppController', 'Controller');
/**
 * AdmissionTypesUsers Controller
 *
 * @property AdmissionTypesUser $AdmissionTypesUser
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class AdmissionTypesUsersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->AdmissionTypesUser->recursive = 0;
		$this->set('admissionTypesUsers', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->AdmissionTypesUser->exists($id)) {
			throw new NotFoundException(__('Invalid admission types user'));
		}
		$options = array('conditions' => array('AdmissionTypesUser.' . $this->AdmissionTypesUser->primaryKey => $id));
		$this->set('admissionTypesUser', $this->AdmissionTypesUser->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->AdmissionTypesUser->create();
			if ($this->AdmissionTypesUser->save($this->request->data)) {
				$this->Session->setFlash(__('The admission types user has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The admission types user could not be saved. Please, try again.'));
			}
		}
		$users = $this->AdmissionTypesUser->User->find('list');
		$admissionTypes = $this->AdmissionTypesUser->AdmissionType->find('list');
		$this->set(compact('users', 'admissionTypes'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->AdmissionTypesUser->exists($id)) {
			throw new NotFoundException(__('Invalid admission types user'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->AdmissionTypesUser->save($this->request->data)) {
				$this->Session->setFlash(__('The admission types user has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The admission types user could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('AdmissionTypesUser.' . $this->AdmissionTypesUser->primaryKey => $id));
			$this->request->data = $this->AdmissionTypesUser->find('first', $options);
		}
		$users = $this->AdmissionTypesUser->User->find('list');
		$admissionTypes = $this->AdmissionTypesUser->AdmissionType->find('list');
		$this->set(compact('users', 'admissionTypes'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->AdmissionTypesUser->id = $id;
		if (!$this->AdmissionTypesUser->exists()) {
			throw new NotFoundException(__('Invalid admission types user'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->AdmissionTypesUser->delete()) {
			$this->Session->setFlash(__('The admission types user has been deleted.'));
		} else {
			$this->Session->setFlash(__('The admission types user could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
