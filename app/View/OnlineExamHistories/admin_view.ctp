<div class="onlineExamHistories view">
<h2><?php echo __('Online Exam History'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($onlineExamHistory['OnlineExamHistory']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Online Exam'); ?></dt>
		<dd>
			<?php echo $this->Html->link($onlineExamHistory['OnlineExam']['name'], array('controller' => 'online_exams', 'action' => 'view', $onlineExamHistory['OnlineExam']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($onlineExamHistory['User']['name'], array('controller' => 'users', 'action' => 'view', $onlineExamHistory['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Mark'); ?></dt>
		<dd>
			<?php echo h($onlineExamHistory['OnlineExamHistory']['mark']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($onlineExamHistory['OnlineExamHistory']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($onlineExamHistory['OnlineExamHistory']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Online Exam History'), array('action' => 'edit', $onlineExamHistory['OnlineExamHistory']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Online Exam History'), array('action' => 'delete', $onlineExamHistory['OnlineExamHistory']['id']), array(), __('Are you sure you want to delete # %s?', $onlineExamHistory['OnlineExamHistory']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Online Exam Histories'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Online Exam History'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Online Exams'), array('controller' => 'online_exams', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Online Exam'), array('controller' => 'online_exams', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
