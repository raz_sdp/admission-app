<div class="questions view">
<h2><?php echo __('Question'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($question['Question']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Subject'); ?></dt>
		<dd>
			<?php echo $this->Html->link($question['Subject']['name'], array('controller' => 'subjects', 'action' => 'view', $question['Subject']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Admission Type'); ?></dt>
		<dd>
			<?php echo $this->Html->link($question['AdmissionType']['name'], array('controller' => 'admission_types', 'action' => 'view', $question['AdmissionType']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Question'); ?></dt>
		<dd>
			<?php echo h($question['Question']['question']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ans A'); ?></dt>
		<dd>
			<?php echo h($question['Question']['ans_a']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ans B'); ?></dt>
		<dd>
			<?php echo h($question['Question']['ans_b']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ans C'); ?></dt>
		<dd>
			<?php echo h($question['Question']['ans_c']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ans D'); ?></dt>
		<dd>
			<?php echo h($question['Question']['ans_d']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ans E'); ?></dt>
		<dd>
			<?php echo h($question['Question']['ans_e']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cor Ans'); ?></dt>
		<dd>
			<?php echo h($question['Question']['cor_ans']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Explanation'); ?></dt>
		<dd>
			<?php echo h($question['Question']['explanation']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('University'); ?></dt>
		<dd>
			<?php echo $this->Html->link($question['University']['name'], array('controller' => 'universities', 'action' => 'view', $question['University']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Year'); ?></dt>
		<dd>
			<?php echo h($question['Question']['year']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Online Exam'); ?></dt>
		<dd>
			<?php echo $this->Html->link($question['OnlineExam']['name'], array('controller' => 'online_exams', 'action' => 'view', $question['OnlineExam']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($question['Question']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($question['Question']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Question'), array('action' => 'edit', $question['Question']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Question'), array('action' => 'delete', $question['Question']['id']), array(), __('Are you sure you want to delete # %s?', $question['Question']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Questions'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Question'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Subjects'), array('controller' => 'subjects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Subject'), array('controller' => 'subjects', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Admission Types'), array('controller' => 'admission_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Admission Type'), array('controller' => 'admission_types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Universities'), array('controller' => 'universities', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New University'), array('controller' => 'universities', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Online Exams'), array('controller' => 'online_exams', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Online Exam'), array('controller' => 'online_exams', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Lectures'), array('controller' => 'lectures', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Lecture'), array('controller' => 'lectures', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Lectures'); ?></h3>
	<?php if (!empty($question['Lecture'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Lesson Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Lecture'); ?></th>
		<th><?php echo __('Order'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($question['Lecture'] as $lecture): ?>
		<tr>
			<td><?php echo $lecture['id']; ?></td>
			<td><?php echo $lecture['lesson_id']; ?></td>
			<td><?php echo $lecture['name']; ?></td>
			<td><?php echo $lecture['lecture']; ?></td>
			<td><?php echo $lecture['order']; ?></td>
			<td><?php echo $lecture['created']; ?></td>
			<td><?php echo $lecture['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'lectures', 'action' => 'view', $lecture['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'lectures', 'action' => 'edit', $lecture['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'lectures', 'action' => 'delete', $lecture['id']), array(), __('Are you sure you want to delete # %s?', $lecture['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Lecture'), array('controller' => 'lectures', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
