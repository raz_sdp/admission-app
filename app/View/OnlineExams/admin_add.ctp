<?php echo $this->element('menu'); ?>
<div class="onlineExams form">
<?php echo $this->Form->create('OnlineExam'); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Online Exam'); ?></legend>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('starting_at');
		echo $this->Form->input('time');
		echo $this->Form->input('no_ques');
		echo $this->Form->input('negate');
		echo $this->Form->input('marks');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
