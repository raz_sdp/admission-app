<?php
$category_opt = array(
    'Lecture' => 'Lectures',
    'Question' => 'Questions',
    'Other' => 'Others'
);
?>
<?php echo $this->element('menu'); ?>
<div class="documents form">
<?php echo $this->Form->create('Document', array('type'=>'file')); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Document'); ?></legend>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('link', array('type'=>'file'));
		echo $this->Form->input('file_category', array('options'=>$category_opt));
		echo $this->Form->input('subject_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
