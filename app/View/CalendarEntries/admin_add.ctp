<div class="calendarEntries form">
<?php echo $this->Form->create('CalendarEntry'); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Calendar Entry'); ?></legend>
	<?php
		echo $this->Form->input('user_id');
		echo $this->Form->input('date');
		echo $this->Form->input('time');
		echo $this->Form->input('content');
		echo $this->Form->input('reminder');
		echo $this->Form->input('reminder_text');
		echo $this->Form->input('by_email');
		echo $this->Form->input('by_phone');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Calendar Entries'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
