<?php echo $this->element('menu'); ?>
<div class="cmsUsers form">
<?php echo $this->Form->create('CmsUser'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Cms User'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('username');
		echo $this->Form->input('email');
		echo $this->Form->input('password');
		echo $this->Form->input('simple_pwd');
		echo $this->Form->input('role_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
