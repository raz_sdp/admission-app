<div class="roles view">
<h2><?php echo __('Role'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($role['Role']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($role['Role']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($role['Role']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($role['Role']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Role'), array('action' => 'edit', $role['Role']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Role'), array('action' => 'delete', $role['Role']['id']), array(), __('Are you sure you want to delete # %s?', $role['Role']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Roles'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Role'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Cms Users'), array('controller' => 'cms_users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cms User'), array('controller' => 'cms_users', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Cms Users'); ?></h3>
	<?php if (!empty($role['CmsUser'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Username'); ?></th>
		<th><?php echo __('Email'); ?></th>
		<th><?php echo __('Password'); ?></th>
		<th><?php echo __('Simple Pwd'); ?></th>
		<th><?php echo __('Role Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($role['CmsUser'] as $cmsUser): ?>
		<tr>
			<td><?php echo $cmsUser['id']; ?></td>
			<td><?php echo $cmsUser['username']; ?></td>
			<td><?php echo $cmsUser['email']; ?></td>
			<td><?php echo $cmsUser['password']; ?></td>
			<td><?php echo $cmsUser['simple_pwd']; ?></td>
			<td><?php echo $cmsUser['role_id']; ?></td>
			<td><?php echo $cmsUser['created']; ?></td>
			<td><?php echo $cmsUser['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'cms_users', 'action' => 'view', $cmsUser['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'cms_users', 'action' => 'edit', $cmsUser['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'cms_users', 'action' => 'delete', $cmsUser['id']), array(), __('Are you sure you want to delete # %s?', $cmsUser['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Cms User'), array('controller' => 'cms_users', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
