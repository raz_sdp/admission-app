<div class="calendarEntries index">
	<h2><?php echo __('Calendar Entries'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('date'); ?></th>
			<th><?php echo $this->Paginator->sort('time'); ?></th>
			<th><?php echo $this->Paginator->sort('content'); ?></th>
			<th><?php echo $this->Paginator->sort('reminder'); ?></th>
			<th><?php echo $this->Paginator->sort('reminder_text'); ?></th>
			<th><?php echo $this->Paginator->sort('by_email'); ?></th>
			<th><?php echo $this->Paginator->sort('by_phone'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($calendarEntries as $calendarEntry): ?>
	<tr>
		<td><?php echo h($calendarEntry['CalendarEntry']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($calendarEntry['User']['name'], array('controller' => 'users', 'action' => 'view', $calendarEntry['User']['id'])); ?>
		</td>
		<td><?php echo h($calendarEntry['CalendarEntry']['date']); ?>&nbsp;</td>
		<td><?php echo h($calendarEntry['CalendarEntry']['time']); ?>&nbsp;</td>
		<td><?php echo h($calendarEntry['CalendarEntry']['content']); ?>&nbsp;</td>
		<td><?php echo h($calendarEntry['CalendarEntry']['reminder']); ?>&nbsp;</td>
		<td><?php echo h($calendarEntry['CalendarEntry']['reminder_text']); ?>&nbsp;</td>
		<td><?php echo h($calendarEntry['CalendarEntry']['by_email']); ?>&nbsp;</td>
		<td><?php echo h($calendarEntry['CalendarEntry']['by_phone']); ?>&nbsp;</td>
		<td><?php echo h($calendarEntry['CalendarEntry']['created']); ?>&nbsp;</td>
		<td><?php echo h($calendarEntry['CalendarEntry']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $calendarEntry['CalendarEntry']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $calendarEntry['CalendarEntry']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $calendarEntry['CalendarEntry']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $calendarEntry['CalendarEntry']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Calendar Entry'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
