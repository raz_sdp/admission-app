<!DOCTYPE html>
<html lang="en">
<head>
	<title>Ragistration</title>	
	
	<style type="text/css">
	body {
    background:url(<?php echo $this->webroot?>img/app_background.png) no-repeat;
    background-size: 100% 100%;
}



.sign-form h1 {
    font-size: 36px;
    font-family:Arial;
    padding: 0px;
    margin: 0px;
    height: 62px;
    color:#ffffff;
    text-align: center;
    border-top-left-radius: 5px;
    border-top-right-radius: 5px;
    padding-top: 10px;
}

#registration-form  {
        width:80%;
    margin: auto;
    margin-top: 20px;
}
.sign-form {    
    margin:auto;
    margin-top: 50px;
    width: 100%;
    border-radius: 5px;
    height: 580px;
}
.btn-info   {
    width: 100%;
}

.form-box h1 span {
    font-weight: lighter;
}

.alert {
     padding: 0px; 
     margin-bottom: 0px; 
    border: 1px solid transparent;
    border-radius: 4px;
}
.form-group {
    margin-bottom: 15px;
}
  
	</style>

</head>
<body>
	<div class="">
	<div class="row sign-form">
		
        <h1>SIGN <span>UP</span></h1>
           
    	    <form  data-toggle="validator" role="form" id="registration-form" method="post" action="<?php echo $this->webroot?>users/register">
            <!--User name field -->
                <div class="form-group  has-feedback">               
                    <div class="input-group">
                        <div class="input-group-addon"><span class="glyphicon glyphicon-user"></span></div>
                        <input type="text" class="form-control" id="form-name-field" value="" placeholder="Full Name" name="data[User][name]" required>                        
                    </div>
                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>                    
                </div>
            
            <!--name field-->
            <div class="form-group has-feedback">                
                    <div class="input-group">
                        <div class="input-group-addon"><span class="glyphicon glyphicon-user"></span></div>
                        <input type="text" class="form-control" id="form-name-field" value="" placeholder="User Name" name="data[User][username]" required>
                    </div>
                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>  
                </div>
            <!-- email field -->
                <div class="form-group has-feedback">                
                    <div class="input-group">
                        <div class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></div>
                        <input type="email" class="form-control" id="form-email-field" value="" placeholder="Email" name="data[User][mail]">
                    </div>
                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>  
                </div>
            <!--college field-->
            <div class="form-group has-feedback">                
                    <div class="input-group">
                        <div class="input-group-addon"><span class="glyphicon glyphicon-book"></span></div>
                        <input type="text" class="form-control" id="form-phone-field" value="" placeholder="College" name="data[User][college]" required>
                    </div>
                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>     
                </div>
            <!--address-1 field-->
            <div class="form-group has-feedback">                
                    <div class="input-group">
                        <div class="input-group-addon"><span class="glyphicon glyphicon-map-marker"></span></div>
                        <input type="text" class="form-control" id="form-phone-field" value="" placeholder="Address" name="data[User][address_1]" required>
                    </div>
                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>     
                </div>
             <!--City field-->
            <div class="form-group has-feedback">                
                    <div class="input-group">
                        <div class="input-group-addon"><span class="glyphicon glyphicon-map-marker"></span></div>
                        <input type="text" class="form-control" id="form-phone-field" value="" placeholder="City" name="data[User][city]" required>
                    </div>
                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>     
                </div>
            
           
            <!-- phone field -->
                <div class="form-group has-feedback">                
                    <div class="input-group">
                        <div class="input-group-addon"><span class="glyphicon glyphicon-phone"></span></div>
                        <input type="phone" class="form-control" pattern="^(?:\+?88)?01[15-9]\d{8}$" id="form-phone-field" value="" placeholder="Mobile" name="data[User][mobile]" required>
                    </div>
                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>     
                </div>
            
            
            <!--password field-->
             <div class="form-group has-feedback">               
                    <div class="input-group">
                        <div class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></div>
                        <input type="password"  class="form-control" id="inputPassword" value="" placeholder="Password" name="data[User][password]" required>
                    </div>
                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>     
                </div>
                <div class="form-group has-feedback">               
                    <div class="input-group">
                        <div class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></div>
                        <input type="password" class="form-control" id="inputPasswordConfirm" value="" placeholder="Confirm Password" data-match="#inputPassword" data-match-error="Whoops, these don't match" required>
                    </div>
                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>     
                </div>
                
            
            <div class="form-group">
       		<button type="submit" class="btn btn-info">Sign Up</button>
       	   </div>
    	    </form>
             
		</div>
	</div>
</div>
</body>
</html>