<div class="lecturesQuestions index">
	<h2><?php echo __('Lectures Questions'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('lecture_id'); ?></th>
			<th><?php echo $this->Paginator->sort('question_id'); ?></th>
			<th><?php echo $this->Paginator->sort('example'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($lecturesQuestions as $lecturesQuestion): ?>
	<tr>
		<td><?php echo h($lecturesQuestion['LecturesQuestion']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($lecturesQuestion['Lecture']['name'], array('controller' => 'lectures', 'action' => 'view', $lecturesQuestion['Lecture']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($lecturesQuestion['Question']['id'], array('controller' => 'questions', 'action' => 'view', $lecturesQuestion['Question']['id'])); ?>
		</td>
		<td><?php echo h($lecturesQuestion['LecturesQuestion']['example']); ?>&nbsp;</td>
		<td><?php echo h($lecturesQuestion['LecturesQuestion']['created']); ?>&nbsp;</td>
		<td><?php echo h($lecturesQuestion['LecturesQuestion']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $lecturesQuestion['LecturesQuestion']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $lecturesQuestion['LecturesQuestion']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $lecturesQuestion['LecturesQuestion']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $lecturesQuestion['LecturesQuestion']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Lectures Question'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Lectures'), array('controller' => 'lectures', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Lecture'), array('controller' => 'lectures', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Questions'), array('controller' => 'questions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Question'), array('controller' => 'questions', 'action' => 'add')); ?> </li>
	</ul>
</div>
