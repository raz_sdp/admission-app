<?php
App::uses('AppModel', 'Model');
/**
 * Question Model
 *
 * @property Subject $Subject
 * @property AdmissionType $AdmissionType
 * @property University $University
 * @property OnlineExam $OnlineExam
 * @property Lecture $Lecture
 */
class Question extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Subject' => array(
			'className' => 'Subject',
			'foreignKey' => 'subject_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'AdmissionType' => array(
			'className' => 'AdmissionType',
			'foreignKey' => 'admission_type_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'University' => array(
			'className' => 'University',
			'foreignKey' => 'university_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'OnlineExam' => array(
			'className' => 'OnlineExam',
			'foreignKey' => 'online_exam_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'Lecture' => array(
			'className' => 'Lecture',
			'joinTable' => 'lectures_questions',
			'foreignKey' => 'question_id',
			'associationForeignKey' => 'lecture_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		)
	);

}
