<?php echo $this->element('menu'); ?>
<div class="questions index">
	<h2><?php echo __('Questions'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th>subject_id</th>
			<th>question</th>
			<th>ans_a</th>
			<th>ans_b</th>
			<th>ans_c</th>
			<th>ans_d</th>
            <th>ans_e</th>
			<th>cor_ans</th>
			<th>created</th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($online_exam['Question'] as $question): ?>
	<tr>
		<td><?php echo h($question['Subject']['name']); ?>&nbsp;</td>
		<td><?php echo h($question['question']); ?>&nbsp;</td>
		<td><?php echo h($question['ans_a']); ?>&nbsp;</td>
		<td><?php echo h($question['ans_b']); ?>&nbsp;</td>
		<td><?php echo h($question['ans_c']); ?>&nbsp;</td>
		<td><?php echo h($question['ans_d']); ?>&nbsp;</td>
		<td><?php echo h($question['ans_e']); ?>&nbsp;</td>
		<td><?php echo h($question['cor_ans']); ?>&nbsp;</td>
		<td><?php echo h($question['created']); ?>&nbsp;</td>

        <td class="actions">
			<?php echo $this->Html->link(__('Edit'), array('controller' => 'questions', 'action' => 'edit', $question['id'], $online_exam['OnlineExam']['id'])); ?>
            <?php echo $this->Form->postLink(__('Remove'), array('controller' => 'questions', 'action' => 'remove_from_exam', $question['id'], $online_exam['OnlineExam']['id']), array('confirm' => __('Are you sure you want to remove # %s from this exam?', $question['id']))); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'questions', 'action' => 'delete', $question['id'], $online_exam['OnlineExam']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $question['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>

    <?php echo $this->Html->link(__('Add Question'), array('controller' => 'questions', 'action' => 'add', $online_exam['OnlineExam']['id']), array('class'=>'btn btn-3 btn-3e icon-arrow-right')); ?>
    <?php echo $this->Html->link(__('Upload CSV'), array('controller' => 'questions', 'action' => 'upload_csv', $online_exam['OnlineExam']['id']), array('class'=>'btn btn-3 btn-3e icon-file')); ?>
</div>
