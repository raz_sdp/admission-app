<div class="calendarEntries view">
<h2><?php echo __('Calendar Entry'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($calendarEntry['CalendarEntry']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($calendarEntry['User']['name'], array('controller' => 'users', 'action' => 'view', $calendarEntry['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Date'); ?></dt>
		<dd>
			<?php echo h($calendarEntry['CalendarEntry']['date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Time'); ?></dt>
		<dd>
			<?php echo h($calendarEntry['CalendarEntry']['time']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Content'); ?></dt>
		<dd>
			<?php echo h($calendarEntry['CalendarEntry']['content']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Reminder'); ?></dt>
		<dd>
			<?php echo h($calendarEntry['CalendarEntry']['reminder']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Reminder Text'); ?></dt>
		<dd>
			<?php echo h($calendarEntry['CalendarEntry']['reminder_text']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('By Email'); ?></dt>
		<dd>
			<?php echo h($calendarEntry['CalendarEntry']['by_email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('By Phone'); ?></dt>
		<dd>
			<?php echo h($calendarEntry['CalendarEntry']['by_phone']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($calendarEntry['CalendarEntry']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($calendarEntry['CalendarEntry']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Calendar Entry'), array('action' => 'edit', $calendarEntry['CalendarEntry']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Calendar Entry'), array('action' => 'delete', $calendarEntry['CalendarEntry']['id']), array(), __('Are you sure you want to delete # %s?', $calendarEntry['CalendarEntry']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Calendar Entries'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Calendar Entry'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
