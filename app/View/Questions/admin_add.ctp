<?php echo $this->element('menu'); ?>
<?php
$cor_ans_opt = array(
    'a' => 'A',
    'b' => 'B',
    'c' => 'C',
    'd' => 'D',
    'e' => 'E',
);
?>
<div class="questions form">
<?php echo $this->Form->create('Question'); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Question'); ?></legend>
	<?php
		echo $this->Form->input('subject_id');
		echo $this->Form->input('admission_type_id');
		echo $this->Form->input('question');
		echo $this->Form->input('ans_a');
		echo $this->Form->input('ans_b');
		echo $this->Form->input('ans_c');
		echo $this->Form->input('ans_d');
		echo $this->Form->input('ans_e');
		echo $this->Form->input('cor_ans', array('options'=>$cor_ans_opt));
		echo $this->Form->input('explanation');
        if(!isset($exam_id)) {
            echo $this->Form->input('university_id');
            echo $this->Form->input('year');
            echo $this->Form->input('online_exam_id');
            echo $this->Form->input('lecture');

        } else {
            echo $this->Form->hidden('online_exam_id', array('value'=>$exam_id));
        }
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
