<?php echo $this->element('menu'); ?>
<div class="onlineExams index">
	<h2><?php echo __('Online Exams'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('starting_at'); ?></th>
			<th><?php echo $this->Paginator->sort('time'); ?></th>
			<th><?php echo $this->Paginator->sort('no_ques'); ?></th>
			<th><?php echo $this->Paginator->sort('negate'); ?></th>
			<th><?php echo $this->Paginator->sort('marks'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($onlineExams as $onlineExam): ?>
	<tr>
		<td><?php echo h($onlineExam['OnlineExam']['name']); ?>&nbsp;</td>
        <td><?php echo h($onlineExam['OnlineExam']['starting_at']); ?>
        <?php
            $time = new DateTime($onlineExam['OnlineExam']['starting_at']);
            $time->add(new DateInterval('PT' . $onlineExam['OnlineExam']['time'] . 'M'));
            $now = new DateTime();
            if($time < $now) {
                echo "(Completed.)<br>";
                echo $this->Html->link(__('View Results'), array('action' => 'results', $onlineExam['OnlineExam']['id']));
            }
        ?>
		&nbsp;</td>
		<td><?php echo h($onlineExam['OnlineExam']['time']); ?>&nbsp;</td>
		<td><?php echo h($onlineExam['OnlineExam']['no_ques']); ?>&nbsp;</td>
		<td><?php echo h($onlineExam['OnlineExam']['negate']); ?>&nbsp;</td>
		<td><?php echo h($onlineExam['OnlineExam']['marks']); ?>&nbsp;</td>
		<td class="actions">
            <?php echo $this->Html->link(__('Questions'), array('action' => 'questions', $onlineExam['OnlineExam']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $onlineExam['OnlineExam']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $onlineExam['OnlineExam']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $onlineExam['OnlineExam']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
