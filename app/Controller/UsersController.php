<?php
App::uses('AppController', 'Controller');

class UsersController extends AppController {

	public $components = array('Paginator', 'Session');

    public function beforeFilter(){
        $this->Auth->allow('my_exams');
    }

	public function admin_index() {
		$this->User->recursive = 0;
		$this->set('users', $this->Paginator->paginate());
	}

	public function admin_view($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
	}

	public function admin_add() {
		if ($this->request->is('post')) {
			$this->User->create();
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The user has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		}
		$admissionTypes = $this->User->AdmissionType->find('list');
		$badges = $this->User->Badge->find('list');
		$this->set(compact('admissionTypes', 'badges'));
	}

	public function admin_edit($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The user has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}
		$admissionTypes = $this->User->AdmissionType->find('list');
		$badges = $this->User->Badge->find('list');
		$this->set(compact('admissionTypes', 'badges'));
	}

	public function admin_delete($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->User->delete()) {
			$this->Session->setFlash(__('The user has been deleted.'));
		} else {
			$this->Session->setFlash(__('The user could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

    public function login(){
        header('Content-Type: application/json');
        $this->autoLayout = false;
        $this->autRender = false;
        if ($this->request->is('post')) {
            $username = $this->request->data['User']['username'];
            $password = $this->request->data['User']['password'];
            $is_user = $this->User->findByUsernameAndPassword($username,$password);
            if(!empty($is_user)){
                die(json_encode(array('success'=>true, 'userdata' => $is_user['User'])));
            } else{
                die(json_encode(array('success'=>false, 'msg' => 'Username and password is incorrect.')));
            }
        } else{
            die(json_encode(array('success'=>false, 'msg' => 'Sorry!Inavalid request.')));
        }
    }

    public function register(){
        $this->layout = 'public';
        //$this->autRender = false;
        if ($this->request->is('post')) {
            $this->User->create();
            if($this->User->save($this->request->data)){
                $this->Session->setFlash(__('Registration successful. Please login and attend the next model Test Exam.'));
            }
        }
    }

    public function my_exams($id) {
        header('Content-Type: application/json');
        $this->autoLayout = false;
        $this->autRender = false;

        $this->User->id = $id;
        if (!$this->User->exists()) {
            die(json_encode(array('success'=>false, 'msg' => 'Sorry! Invalid User.')));
        }

        $options = array(
            'conditions' => array('OnlineExamHistory.user_id' => $id)
        );
        $data = $this->User->OnlineExamHistory->find('all', $options);
        $attended_exams = count($data);
        $user = $data[0]['User'];
        $result_list = array();
        foreach($data as $key => $item) {
            $result_list[$key]['exam_name'] = $item['OnlineExam']['name'];
            $result_list[$key]['exam_id'] = $item['OnlineExam']['id'];
            $result_list[$key]['exam_date'] = $item['OnlineExam']['starting_at'];
            $result_list[$key]['mark'] = $item['OnlineExamHistory']['mark'];
        }
        die(json_encode(array('success'=>true, 'user' => $user, 'attended_exams'=>$attended_exams, 'result_list'=>$result_list)));
    }
}
