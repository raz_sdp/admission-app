<?php
App::uses('AppController', 'Controller');
/**
 * LabelsPosts Controller
 *
 * @property LabelsPost $LabelsPost
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class LabelsPostsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->LabelsPost->recursive = 0;
		$this->set('labelsPosts', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->LabelsPost->exists($id)) {
			throw new NotFoundException(__('Invalid labels post'));
		}
		$options = array('conditions' => array('LabelsPost.' . $this->LabelsPost->primaryKey => $id));
		$this->set('labelsPost', $this->LabelsPost->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->LabelsPost->create();
			if ($this->LabelsPost->save($this->request->data)) {
				$this->Session->setFlash(__('The labels post has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The labels post could not be saved. Please, try again.'));
			}
		}
		$posts = $this->LabelsPost->Post->find('list');
		$labels = $this->LabelsPost->Label->find('list');
		$this->set(compact('posts', 'labels'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->LabelsPost->exists($id)) {
			throw new NotFoundException(__('Invalid labels post'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->LabelsPost->save($this->request->data)) {
				$this->Session->setFlash(__('The labels post has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The labels post could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('LabelsPost.' . $this->LabelsPost->primaryKey => $id));
			$this->request->data = $this->LabelsPost->find('first', $options);
		}
		$posts = $this->LabelsPost->Post->find('list');
		$labels = $this->LabelsPost->Label->find('list');
		$this->set(compact('posts', 'labels'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->LabelsPost->id = $id;
		if (!$this->LabelsPost->exists()) {
			throw new NotFoundException(__('Invalid labels post'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->LabelsPost->delete()) {
			$this->Session->setFlash(__('The labels post has been deleted.'));
		} else {
			$this->Session->setFlash(__('The labels post could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
