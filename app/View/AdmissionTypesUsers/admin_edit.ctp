<div class="admissionTypesUsers form">
<?php echo $this->Form->create('AdmissionTypesUser'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Admission Types User'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('user_id');
		echo $this->Form->input('admission_type_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('AdmissionTypesUser.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('AdmissionTypesUser.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Admission Types Users'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Admission Types'), array('controller' => 'admission_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Admission Type'), array('controller' => 'admission_types', 'action' => 'add')); ?> </li>
	</ul>
</div>
