<div class="user-login">
    <?php echo $this->Form->create('CmsUser'); ?>
    <fieldset>
        <legend><?php echo __('Cms User Login'); ?></legend>
        <?php
        echo $this->Form->input('username');
        echo $this->Form->input('password');
        ?>
    </fieldset>
    <?php echo $this->Form->end(__('Login')); ?>
</div>