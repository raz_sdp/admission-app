<?php
App::uses('AppController', 'Controller');

class DocumentsController extends AppController {

	public $components = array('Paginator', 'Session');

    public function beforeFilter(){
        $this->Auth->allow('get_files');
    }

	public function admin_index() {
		$this->Document->recursive = 0;
		$this->set('documents', $this->Paginator->paginate());
	}

	public function admin_add() {
		if ($this->request->is('post')) {
            if(!empty($this->request->data['Document']['link'])) {
                $target_file = 'files/' . basename($this->request->data['Document']['link']['name']);
                #AuthComponent::_setTrace($target_file);
                if (move_uploaded_file($this->request->data['Document']['link']["tmp_name"], $target_file)) {
                    $this->request->data['Document']['link'] = $target_file;
                } else {
                    throw new NotFoundException(__('Sorry, there was an error uploading your file.'));
                }
            } else {
                unset($this->request->data['Document']['link']);
            }

            $this->Document->create();
            if ($this->Document->save($this->request->data)) {
				$this->Session->setFlash(__('The document has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The document could not be saved. Please, try again.'));
			}
		}
		$subjects = $this->Document->Subject->find('list');
		$this->set(compact('subjects'));
	}

	public function admin_edit($id = null) {
		if (!$this->Document->exists($id)) {
			throw new NotFoundException(__('Invalid document'));
		}
		if ($this->request->is(array('post', 'put'))) {
            if(!empty($this->request->data['Document']['link'])) {
                if(!empty($this->request->data['Document']['link']['name'])) {
                    $target_file = 'files/' . basename($this->request->data['Document']['link']['name']);
                    #AuthComponent::_setTrace($target_file);
                    if (move_uploaded_file($this->request->data['Document']['link']["tmp_name"], $target_file)) {
                        $this->request->data['Document']['link'] = $target_file;
                    } else {
                        throw new NotFoundException(__('Sorry, there was an error uploading your file.'));
                    }
                } else {
                    $options = array('conditions' => array('Document.' . $this->Document->primaryKey => $id));
                    $data = $this->Document->find('first', $options);
                    $this->request->data['Document']['link'] = $data['Document']['link'];
                }
            } else {
                unset($this->request->data['Document']['link']);
            }

            $this->Document->id = $id;
			if ($this->Document->save($this->request->data)) {
				$this->Session->setFlash(__('The document has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The document could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Document.' . $this->Document->primaryKey => $id));
			$this->request->data = $this->Document->find('first', $options);
		}
		$subjects = $this->Document->Subject->find('list');
		$this->set(compact('subjects'));
	}

	public function admin_delete($id = null) {
		$this->Document->id = $id;
		if (!$this->Document->exists()) {
			throw new NotFoundException(__('Invalid document'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Document->delete()) {
			$this->Session->setFlash(__('The document has been deleted.'));
		} else {
			$this->Session->setFlash(__('The document could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

    public function get_files($id = null) {
        header('Content-Type: application/json');
        $this->autoLayout = false;
        $this->autRender = false;

        $this->Document->recursive = 1;
        $documents = $this->Document->find('all');

        foreach($documents as $key => $item) {
            $documents[$key]['Document']['subject'] = $item['Subject']['name'];
            $documents[$key]['Document']['link'] = FULL_BASE_URL . $this->webroot . $item['Document']['link'];
            unset($documents[$key]['Document']['subject_id'], $documents[$key]['Subject']);
        }

        die(json_encode(array('success'=>true, 'documents' => Hash::extract($documents, '{n}.Document'))));
    }
}
