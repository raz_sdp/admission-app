<?php
App::uses('AppModel', 'Model');
/**
 * CompletanceHistory Model
 *
 * @property User $User
 * @property Lecture $Lecture
 */
class CompletanceHistory extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'completance_history';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Lecture' => array(
			'className' => 'Lecture',
			'foreignKey' => 'lecture_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
