<div class="onlineExamHistories form">
<?php echo $this->Form->create('OnlineExamHistory'); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Online Exam History'); ?></legend>
	<?php
		echo $this->Form->input('online_exam_id');
		echo $this->Form->input('user_id');
		echo $this->Form->input('mark');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Online Exam Histories'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Online Exams'), array('controller' => 'online_exams', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Online Exam'), array('controller' => 'online_exams', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
