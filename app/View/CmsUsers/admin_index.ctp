<?php echo $this->element('menu'); ?>
<div class="cmsUsers index">
	<h2><?php echo __('Cms Users'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('username'); ?></th>
			<th><?php echo $this->Paginator->sort('email'); ?></th>
			<th><?php echo $this->Paginator->sort('simple_pwd'); ?></th>
			<th><?php echo $this->Paginator->sort('role_id'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($cmsUsers as $cmsUser): ?>
	<tr>
		<td><?php echo h($cmsUser['CmsUser']['username']); ?>&nbsp;</td>
		<td><?php echo h($cmsUser['CmsUser']['email']); ?>&nbsp;</td>
		<td><?php echo h($cmsUser['CmsUser']['simple_pwd']); ?>&nbsp;</td>
		<td><?php echo $cmsUser['Role']['name']; ?>&nbsp;</td>
		<td><?php echo h($cmsUser['CmsUser']['created']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $cmsUser['CmsUser']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $cmsUser['CmsUser']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $cmsUser['CmsUser']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
