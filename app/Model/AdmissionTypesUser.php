<?php
App::uses('AppModel', 'Model');
/**
 * AdmissionTypesUser Model
 *
 * @property User $User
 * @property AdmissionType $AdmissionType
 */
class AdmissionTypesUser extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'AdmissionType' => array(
			'className' => 'AdmissionType',
			'foreignKey' => 'admission_type_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
