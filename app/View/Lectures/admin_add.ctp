<div class="lectures form">
<?php echo $this->Form->create('Lecture'); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Lecture'); ?></legend>
	<?php
		echo $this->Form->input('lesson_id');
		echo $this->Form->input('name');
		echo $this->Form->input('lecture');
		echo $this->Form->input('order');
		echo $this->Form->input('Question');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Lectures'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Lessons'), array('controller' => 'lessons', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Lesson'), array('controller' => 'lessons', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Completance Histories'), array('controller' => 'completance_histories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Completance History'), array('controller' => 'completance_histories', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Questions'), array('controller' => 'questions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Question'), array('controller' => 'questions', 'action' => 'add')); ?> </li>
	</ul>
</div>
