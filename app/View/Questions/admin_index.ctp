<div class="questions index">
	<h2><?php echo __('Questions'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('subject_id'); ?></th>
			<th><?php echo $this->Paginator->sort('admission_type_id'); ?></th>
			<th><?php echo $this->Paginator->sort('question'); ?></th>
			<th><?php echo $this->Paginator->sort('ans_a'); ?></th>
			<th><?php echo $this->Paginator->sort('ans_b'); ?></th>
			<th><?php echo $this->Paginator->sort('ans_c'); ?></th>
			<th><?php echo $this->Paginator->sort('ans_d'); ?></th>
			<th><?php echo $this->Paginator->sort('ans_e'); ?></th>
			<th><?php echo $this->Paginator->sort('cor_ans'); ?></th>
			<th><?php echo $this->Paginator->sort('explanation'); ?></th>
			<th><?php echo $this->Paginator->sort('university_id'); ?></th>
			<th><?php echo $this->Paginator->sort('year'); ?></th>
			<th><?php echo $this->Paginator->sort('online_exam_id'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($questions as $question): ?>
	<tr>
		<td><?php echo h($question['Question']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($question['Subject']['name'], array('controller' => 'subjects', 'action' => 'view', $question['Subject']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($question['AdmissionType']['name'], array('controller' => 'admission_types', 'action' => 'view', $question['AdmissionType']['id'])); ?>
		</td>
		<td><?php echo h($question['Question']['question']); ?>&nbsp;</td>
		<td><?php echo h($question['Question']['ans_a']); ?>&nbsp;</td>
		<td><?php echo h($question['Question']['ans_b']); ?>&nbsp;</td>
		<td><?php echo h($question['Question']['ans_c']); ?>&nbsp;</td>
		<td><?php echo h($question['Question']['ans_d']); ?>&nbsp;</td>
		<td><?php echo h($question['Question']['ans_e']); ?>&nbsp;</td>
		<td><?php echo h($question['Question']['cor_ans']); ?>&nbsp;</td>
		<td><?php echo h($question['Question']['explanation']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($question['University']['name'], array('controller' => 'universities', 'action' => 'view', $question['University']['id'])); ?>
		</td>
		<td><?php echo h($question['Question']['year']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($question['OnlineExam']['name'], array('controller' => 'online_exams', 'action' => 'view', $question['OnlineExam']['id'])); ?>
		</td>
		<td><?php echo h($question['Question']['created']); ?>&nbsp;</td>
		<td><?php echo h($question['Question']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $question['Question']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $question['Question']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $question['Question']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $question['Question']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Question'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Subjects'), array('controller' => 'subjects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Subject'), array('controller' => 'subjects', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Admission Types'), array('controller' => 'admission_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Admission Type'), array('controller' => 'admission_types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Universities'), array('controller' => 'universities', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New University'), array('controller' => 'universities', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Online Exams'), array('controller' => 'online_exams', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Online Exam'), array('controller' => 'online_exams', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Lectures'), array('controller' => 'lectures', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Lecture'), array('controller' => 'lectures', 'action' => 'add')); ?> </li>
	</ul>
</div>
