<?php $onlineExamHistories = $online_exam['OnlineExamHistory']?>
<?php echo $this->element('menu'); ?>
<div class="onlineExamHistories index">
	<h2><?php echo __('Online Exam Result Of '); echo $online_exam['OnlineExam']['name']; ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th>user_id</th>
			<th>mark</th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($onlineExamHistories as $onlineExamHistory): ?>
	<tr>
		<td>
			<?php echo $this->Html->link($onlineExamHistory['User']['name'], array('controller' => 'users', 'action' => 'view', $onlineExamHistory['User']['id'])); ?>
		</td>
		<td><?php echo h($onlineExamHistory['mark']); ?>&nbsp;</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
</div>
