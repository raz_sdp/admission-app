<div class="universities form">
<?php echo $this->Form->create('University'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit University'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('website');
		echo $this->Form->input('address');
		echo $this->Form->input('city');
		echo $this->Form->input('description');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('University.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('University.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Universities'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Questions'), array('controller' => 'questions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Question'), array('controller' => 'questions', 'action' => 'add')); ?> </li>
	</ul>
</div>
