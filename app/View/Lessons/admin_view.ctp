<div class="lessons view">
<h2><?php echo __('Lesson'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($lesson['Lesson']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Section'); ?></dt>
		<dd>
			<?php echo $this->Html->link($lesson['Section']['name'], array('controller' => 'sections', 'action' => 'view', $lesson['Section']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($lesson['Lesson']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($lesson['Lesson']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($lesson['Lesson']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Lesson'), array('action' => 'edit', $lesson['Lesson']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Lesson'), array('action' => 'delete', $lesson['Lesson']['id']), array(), __('Are you sure you want to delete # %s?', $lesson['Lesson']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Lessons'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Lesson'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sections'), array('controller' => 'sections', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Section'), array('controller' => 'sections', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Lectures'), array('controller' => 'lectures', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Lecture'), array('controller' => 'lectures', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Lectures'); ?></h3>
	<?php if (!empty($lesson['Lecture'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Lesson Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Lecture'); ?></th>
		<th><?php echo __('Order'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($lesson['Lecture'] as $lecture): ?>
		<tr>
			<td><?php echo $lecture['id']; ?></td>
			<td><?php echo $lecture['lesson_id']; ?></td>
			<td><?php echo $lecture['name']; ?></td>
			<td><?php echo $lecture['lecture']; ?></td>
			<td><?php echo $lecture['order']; ?></td>
			<td><?php echo $lecture['created']; ?></td>
			<td><?php echo $lecture['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'lectures', 'action' => 'view', $lecture['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'lectures', 'action' => 'edit', $lecture['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'lectures', 'action' => 'delete', $lecture['id']), array(), __('Are you sure you want to delete # %s?', $lecture['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Lecture'), array('controller' => 'lectures', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
