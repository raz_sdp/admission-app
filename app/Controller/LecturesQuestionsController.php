<?php
App::uses('AppController', 'Controller');
/**
 * LecturesQuestions Controller
 *
 * @property LecturesQuestion $LecturesQuestion
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class LecturesQuestionsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->LecturesQuestion->recursive = 0;
		$this->set('lecturesQuestions', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->LecturesQuestion->exists($id)) {
			throw new NotFoundException(__('Invalid lectures question'));
		}
		$options = array('conditions' => array('LecturesQuestion.' . $this->LecturesQuestion->primaryKey => $id));
		$this->set('lecturesQuestion', $this->LecturesQuestion->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->LecturesQuestion->create();
			if ($this->LecturesQuestion->save($this->request->data)) {
				$this->Session->setFlash(__('The lectures question has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The lectures question could not be saved. Please, try again.'));
			}
		}
		$lectures = $this->LecturesQuestion->Lecture->find('list');
		$questions = $this->LecturesQuestion->Question->find('list');
		$this->set(compact('lectures', 'questions'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->LecturesQuestion->exists($id)) {
			throw new NotFoundException(__('Invalid lectures question'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->LecturesQuestion->save($this->request->data)) {
				$this->Session->setFlash(__('The lectures question has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The lectures question could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('LecturesQuestion.' . $this->LecturesQuestion->primaryKey => $id));
			$this->request->data = $this->LecturesQuestion->find('first', $options);
		}
		$lectures = $this->LecturesQuestion->Lecture->find('list');
		$questions = $this->LecturesQuestion->Question->find('list');
		$this->set(compact('lectures', 'questions'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->LecturesQuestion->id = $id;
		if (!$this->LecturesQuestion->exists()) {
			throw new NotFoundException(__('Invalid lectures question'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->LecturesQuestion->delete()) {
			$this->Session->setFlash(__('The lectures question has been deleted.'));
		} else {
			$this->Session->setFlash(__('The lectures question could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
