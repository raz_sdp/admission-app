<?php

App::uses('Controller', 'Controller');

class AppController extends Controller {

    public $components = array(
        'Session', 'RequestHandler','Cookie',
        'Auth' => array(
            'loginAction' => array('controller' => 'cms_users', 'action' => 'login', 'admin' => true),
            'loginRedirect' => array('controller' => 'cms_users', 'action' => 'index', 'admin' => true),
            'logoutRedirect' => array('controller' => 'cms_users', 'action' => 'logout', 'admin' => true),
            'authError' => 'You are not allowed',
            'authenticate' => array(
                'Form' => array(
                    'fields' => array('username' => 'username', 'password' => 'password')
                )
            )
        )
    );

    public function beforeFilter(){
        $this->Auth->allow();
    }
}
