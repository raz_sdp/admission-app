<?php echo $this->element('menu');?>
<div class="questions form">
    <?php echo $this->Form->create('Question', array('type' => 'file')); ?>
    <fieldset>
        <legend><?php echo __('Admin Upload CSV of question'); ?></legend>
        <?php
        echo $this->Form->input('filename', array('type' => 'file', 'accept' => '.csv'));

        /*
            For Excel Files 97-2003 (.xls), use:
                accept="application/vnd.ms-excel"
            For Excel Files 2007+ (.xlsx), use:
                accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"

        */
        ?>
    </fieldset>
    <?php echo $this->Form->end(__('Submit')); ?>
</div>