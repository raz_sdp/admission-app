<div class="lecturesQuestions form">
<?php echo $this->Form->create('LecturesQuestion'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Lectures Question'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('lecture_id');
		echo $this->Form->input('question_id');
		echo $this->Form->input('example');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('LecturesQuestion.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('LecturesQuestion.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Lectures Questions'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Lectures'), array('controller' => 'lectures', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Lecture'), array('controller' => 'lectures', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Questions'), array('controller' => 'questions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Question'), array('controller' => 'questions', 'action' => 'add')); ?> </li>
	</ul>
</div>
