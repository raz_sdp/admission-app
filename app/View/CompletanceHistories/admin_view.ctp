<div class="completanceHistories view">
<h2><?php echo __('Completance History'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($completanceHistory['CompletanceHistory']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($completanceHistory['User']['name'], array('controller' => 'users', 'action' => 'view', $completanceHistory['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Lecture'); ?></dt>
		<dd>
			<?php echo $this->Html->link($completanceHistory['Lecture']['name'], array('controller' => 'lectures', 'action' => 'view', $completanceHistory['Lecture']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($completanceHistory['CompletanceHistory']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($completanceHistory['CompletanceHistory']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Completance History'), array('action' => 'edit', $completanceHistory['CompletanceHistory']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Completance History'), array('action' => 'delete', $completanceHistory['CompletanceHistory']['id']), array(), __('Are you sure you want to delete # %s?', $completanceHistory['CompletanceHistory']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Completance Histories'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Completance History'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Lectures'), array('controller' => 'lectures', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Lecture'), array('controller' => 'lectures', 'action' => 'add')); ?> </li>
	</ul>
</div>
