<div class="admissionTypesUsers view">
<h2><?php echo __('Admission Types User'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($admissionTypesUser['AdmissionTypesUser']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($admissionTypesUser['User']['name'], array('controller' => 'users', 'action' => 'view', $admissionTypesUser['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Admission Type'); ?></dt>
		<dd>
			<?php echo $this->Html->link($admissionTypesUser['AdmissionType']['name'], array('controller' => 'admission_types', 'action' => 'view', $admissionTypesUser['AdmissionType']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($admissionTypesUser['AdmissionTypesUser']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($admissionTypesUser['AdmissionTypesUser']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Admission Types User'), array('action' => 'edit', $admissionTypesUser['AdmissionTypesUser']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Admission Types User'), array('action' => 'delete', $admissionTypesUser['AdmissionTypesUser']['id']), array(), __('Are you sure you want to delete # %s?', $admissionTypesUser['AdmissionTypesUser']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Admission Types Users'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Admission Types User'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Admission Types'), array('controller' => 'admission_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Admission Type'), array('controller' => 'admission_types', 'action' => 'add')); ?> </li>
	</ul>
</div>
