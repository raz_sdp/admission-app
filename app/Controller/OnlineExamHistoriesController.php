<?php
App::uses('AppController', 'Controller');

class OnlineExamHistoriesController extends AppController {

	public $components = array('Paginator', 'Session');

    public function beforeFilter(){
        $this->Auth->allow('insert_result');
    }

	public function admin_index() {
		$this->OnlineExamHistory->recursive = 0;
		$this->set('onlineExamHistories', $this->Paginator->paginate());
	}

	public function admin_view($id = null) {
		if (!$this->OnlineExamHistory->exists($id)) {
			throw new NotFoundException(__('Invalid online exam history'));
		}
		$options = array('conditions' => array('OnlineExamHistory.' . $this->OnlineExamHistory->primaryKey => $id));
		$this->set('onlineExamHistory', $this->OnlineExamHistory->find('first', $options));
	}

	public function admin_add() {
		if ($this->request->is('post')) {
			$this->OnlineExamHistory->create();
			if ($this->OnlineExamHistory->save($this->request->data)) {
				$this->Session->setFlash(__('The online exam history has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The online exam history could not be saved. Please, try again.'));
			}
		}
		$onlineExams = $this->OnlineExamHistory->OnlineExam->find('list');
		$users = $this->OnlineExamHistory->User->find('list');
		$this->set(compact('onlineExams', 'users'));
	}

	public function admin_edit($id = null) {
		if (!$this->OnlineExamHistory->exists($id)) {
			throw new NotFoundException(__('Invalid online exam history'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->OnlineExamHistory->save($this->request->data)) {
				$this->Session->setFlash(__('The online exam history has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The online exam history could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('OnlineExamHistory.' . $this->OnlineExamHistory->primaryKey => $id));
			$this->request->data = $this->OnlineExamHistory->find('first', $options);
		}
		$onlineExams = $this->OnlineExamHistory->OnlineExam->find('list');
		$users = $this->OnlineExamHistory->User->find('list');
		$this->set(compact('onlineExams', 'users'));
	}

	public function admin_delete($id = null) {
		$this->OnlineExamHistory->id = $id;
		if (!$this->OnlineExamHistory->exists()) {
			throw new NotFoundException(__('Invalid online exam history'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->OnlineExamHistory->delete()) {
			$this->Session->setFlash(__('The online exam history has been deleted.'));
		} else {
			$this->Session->setFlash(__('The online exam history could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

    public function insert_result($online_exam_id=null, $user_id=null, $mark=null) {
        header('Content-Type: application/json');
        $this->autoLayout = false;
        $this->autoRender = false;

        $this->OnlineExamHistory->OnlineExam->id = $online_exam_id;
        if (!$this->OnlineExamHistory->OnlineExam->exists()) {
            die(json_encode(array('success'=>false, 'msg' => 'Sorry! Invalid exam.')));
        }
        $this->OnlineExamHistory->User->id = $user_id;
        if (!$this->OnlineExamHistory->User->exists()) {
            die(json_encode(array('success'=>false, 'msg' => 'Sorry! Invalid user.')));
        }

        $data['OnlineExamHistory']['online_exam_id'] = $online_exam_id;
        $data['OnlineExamHistory']['user_id'] = $user_id;
        $data['OnlineExamHistory']['mark'] = $mark;

        $this->OnlineExamHistory->recursive = -1;
        $is_exist = $this->OnlineExamHistory->find('first', array(
            'conditions' => array(
                'user_id' => $user_id,
                'online_exam_id' => $online_exam_id
            )
        ));
        if(!empty($is_exist)) {
            $this->OnlineExamHistory->id = $is_exist['OnlineExamHistory']['id'];
        } else {
            $this->OnlineExamHistory->create();
        }

        if ($this->OnlineExamHistory->save($data)) {
            die(json_encode(array('success'=>true, 'msg' => 'Mark Saved.')));
        } else {
            die(json_encode(array('success'=>false, 'msg' => 'Sorry! Try again.')));
        }
    }

    public function get_user_history($user_id) {

    }
}
