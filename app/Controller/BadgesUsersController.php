<?php
App::uses('AppController', 'Controller');
/**
 * BadgesUsers Controller
 *
 * @property BadgesUser $BadgesUser
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class BadgesUsersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->BadgesUser->recursive = 0;
		$this->set('badgesUsers', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->BadgesUser->exists($id)) {
			throw new NotFoundException(__('Invalid badges user'));
		}
		$options = array('conditions' => array('BadgesUser.' . $this->BadgesUser->primaryKey => $id));
		$this->set('badgesUser', $this->BadgesUser->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->BadgesUser->create();
			if ($this->BadgesUser->save($this->request->data)) {
				$this->Session->setFlash(__('The badges user has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The badges user could not be saved. Please, try again.'));
			}
		}
		$users = $this->BadgesUser->User->find('list');
		$badges = $this->BadgesUser->Badge->find('list');
		$this->set(compact('users', 'badges'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->BadgesUser->exists($id)) {
			throw new NotFoundException(__('Invalid badges user'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->BadgesUser->save($this->request->data)) {
				$this->Session->setFlash(__('The badges user has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The badges user could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('BadgesUser.' . $this->BadgesUser->primaryKey => $id));
			$this->request->data = $this->BadgesUser->find('first', $options);
		}
		$users = $this->BadgesUser->User->find('list');
		$badges = $this->BadgesUser->Badge->find('list');
		$this->set(compact('users', 'badges'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->BadgesUser->id = $id;
		if (!$this->BadgesUser->exists()) {
			throw new NotFoundException(__('Invalid badges user'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->BadgesUser->delete()) {
			$this->Session->setFlash(__('The badges user has been deleted.'));
		} else {
			$this->Session->setFlash(__('The badges user could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
