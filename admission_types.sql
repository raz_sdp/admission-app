/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50617
Source Host           : localhost:3306
Source Database       : admission

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2016-03-08 13:17:48
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `admission_types`
-- ----------------------------
DROP TABLE IF EXISTS `admission_types`;
CREATE TABLE `admission_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of admission_types
-- ----------------------------

-- ----------------------------
-- Table structure for `admission_types_users`
-- ----------------------------
DROP TABLE IF EXISTS `admission_types_users`;
CREATE TABLE `admission_types_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `admission_type_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of admission_types_users
-- ----------------------------

-- ----------------------------
-- Table structure for `badges`
-- ----------------------------
DROP TABLE IF EXISTS `badges`;
CREATE TABLE `badges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of badges
-- ----------------------------

-- ----------------------------
-- Table structure for `badges_users`
-- ----------------------------
DROP TABLE IF EXISTS `badges_users`;
CREATE TABLE `badges_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `badge_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of badges_users
-- ----------------------------

-- ----------------------------
-- Table structure for `calendar_entries`
-- ----------------------------
DROP TABLE IF EXISTS `calendar_entries`;
CREATE TABLE `calendar_entries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `time` time DEFAULT NULL,
  `content` text CHARACTER SET utf32 COLLATE utf32_unicode_ci,
  `reminder` tinyint(4) DEFAULT NULL,
  `reminder_text` varchar(100) DEFAULT NULL,
  `by_email` tinyint(1) DEFAULT NULL,
  `by_phone` tinyint(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of calendar_entries
-- ----------------------------

-- ----------------------------
-- Table structure for `cms_users`
-- ----------------------------
DROP TABLE IF EXISTS `cms_users`;
CREATE TABLE `cms_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `simple_pwd` varchar(255) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of cms_users
-- ----------------------------

-- ----------------------------
-- Table structure for `comments`
-- ----------------------------
DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `comment` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `accepted` tinyint(4) DEFAULT NULL,
  `like` int(11) DEFAULT NULL,
  `dislike` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of comments
-- ----------------------------

-- ----------------------------
-- Table structure for `completance_history`
-- ----------------------------
DROP TABLE IF EXISTS `completance_history`;
CREATE TABLE `completance_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `lecture_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of completance_history
-- ----------------------------

-- ----------------------------
-- Table structure for `labels`
-- ----------------------------
DROP TABLE IF EXISTS `labels`;
CREATE TABLE `labels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of labels
-- ----------------------------

-- ----------------------------
-- Table structure for `labels_posts`
-- ----------------------------
DROP TABLE IF EXISTS `labels_posts`;
CREATE TABLE `labels_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) DEFAULT NULL,
  `label_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of labels_posts
-- ----------------------------

-- ----------------------------
-- Table structure for `lectures`
-- ----------------------------
DROP TABLE IF EXISTS `lectures`;
CREATE TABLE `lectures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lesson_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `lecture` text,
  `order` int(3) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of lectures
-- ----------------------------

-- ----------------------------
-- Table structure for `lectures_questions`
-- ----------------------------
DROP TABLE IF EXISTS `lectures_questions`;
CREATE TABLE `lectures_questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lecture_id` int(11) DEFAULT NULL,
  `question_id` int(11) DEFAULT NULL,
  `example` tinyint(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of lectures_questions
-- ----------------------------

-- ----------------------------
-- Table structure for `lessons`
-- ----------------------------
DROP TABLE IF EXISTS `lessons`;
CREATE TABLE `lessons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `section_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of lessons
-- ----------------------------

-- ----------------------------
-- Table structure for `online_exam_history`
-- ----------------------------
DROP TABLE IF EXISTS `online_exam_history`;
CREATE TABLE `online_exam_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `online_exam_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `mark` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of online_exam_history
-- ----------------------------

-- ----------------------------
-- Table structure for `online_exams`
-- ----------------------------
DROP TABLE IF EXISTS `online_exams`;
CREATE TABLE `online_exams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `starting_at` datetime DEFAULT NULL,
  `time` int(3) DEFAULT NULL,
  `no_ques` int(3) DEFAULT NULL,
  `negate` tinyint(1) DEFAULT NULL,
  `marks` int(3) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of online_exams
-- ----------------------------

-- ----------------------------
-- Table structure for `posts`
-- ----------------------------
DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `post` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `like` int(11) DEFAULT NULL,
  `dislike` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of posts
-- ----------------------------
INSERT INTO `posts` VALUES ('3', '20', 'Sample question', '1', 'This is a sample question about physics....', null, null, '2015-03-16 00:00:00', null);
INSERT INTO `posts` VALUES ('4', '20', 'Chemistry Question', '2', 'A sample question about chemistry...', null, null, '2015-03-16 00:00:00', null);
INSERT INTO `posts` VALUES ('5', '20', 'General question', '5', 'this is a general question.....', null, null, '2015-03-16 00:00:00', null);
INSERT INTO `posts` VALUES ('7', '19', 'sfjkashdjfkhsjkld', '1', 'fsdjkfhlsdfiowejmnf,.d f,s jsdkfjksdlu sdfo sdjfls sdjfkljsdl fklsdjakl sdkjfkljfkjowerklwe,mds', null, null, '2015-03-17 00:00:00', null);
INSERT INTO `posts` VALUES ('8', '19', 'sfjkashdjfkhsjkld', '1', 'fsdjkfhlsdfiowejmnf,.d f,s jsdkfjksdlu sdfo sdjfls sdjfkljsdl fklsdjakl sdkjfkljfkjowerklwe,mds', null, null, '2015-03-17 00:00:00', null);
INSERT INTO `posts` VALUES ('9', '19', 'sfjkashdjfkhsjkld', '1', 'fsdjkfhlsdfiowejmnf,.d f,s jsdkfjksdlu sdfo sdjfls sdjfkljsdl fklsdjakl sdkjfkljfkjowerklwe,mds', null, null, '2015-03-17 00:00:00', null);
INSERT INTO `posts` VALUES ('14', '3', 'জারণ-বিজারণ', '2', 'জারণ বিজারণের মাধ্যমে যোজনীর হিসাব...\r\n', null, null, '2015-04-27 00:00:00', null);
INSERT INTO `posts` VALUES ('16', '3', 'gravity', '1', 'is it a dimension??', null, null, '2015-04-27 00:00:00', null);
INSERT INTO `posts` VALUES ('17', '3', 'বলবিদ্যা', '3', '', null, null, '2015-04-27 00:00:00', null);
INSERT INTO `posts` VALUES ('18', '3', 'ডিফারেন্সিয়াল ক্যালকুলাস', '3', 'd/dx(2x^2+3x+1) = ?', null, null, '2015-04-27 00:00:00', null);
INSERT INTO `posts` VALUES ('19', '3', 'Nouns and adjective', '4', 'vugijugi...........................................................', null, null, '2015-04-27 00:00:00', null);
INSERT INTO `posts` VALUES ('20', '3', 'gg', '1', 'bnbn', null, null, '2015-05-07 00:00:00', null);

-- ----------------------------
-- Table structure for `questions`
-- ----------------------------
DROP TABLE IF EXISTS `questions`;
CREATE TABLE `questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject_id` int(11) DEFAULT NULL,
  `admission_type_id` int(11) DEFAULT NULL,
  `question` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `ans_a` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `ans_b` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `ans_c` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `ans_d` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `ans_e` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `cor_ans` int(1) DEFAULT NULL,
  `explanation` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `university_id` int(11) DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  `online_exam_id` int(11) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of questions
-- ----------------------------

-- ----------------------------
-- Table structure for `rankings`
-- ----------------------------
DROP TABLE IF EXISTS `rankings`;
CREATE TABLE `rankings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `admission_type_id` int(11) DEFAULT NULL,
  `points` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rankings
-- ----------------------------

-- ----------------------------
-- Table structure for `reports`
-- ----------------------------
DROP TABLE IF EXISTS `reports`;
CREATE TABLE `reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `report` text,
  `user_id` int(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `acknowledged` tinyint(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of reports
-- ----------------------------

-- ----------------------------
-- Table structure for `roles`
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('1', 'admin', '2016-03-04 18:00:24', '2016-03-04 18:00:24');
INSERT INTO `roles` VALUES ('2', 'teacher', '2016-03-04 18:00:24', '2016-03-04 18:00:24');

-- ----------------------------
-- Table structure for `sections`
-- ----------------------------
DROP TABLE IF EXISTS `sections`;
CREATE TABLE `sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sections
-- ----------------------------

-- ----------------------------
-- Table structure for `subjects`
-- ----------------------------
DROP TABLE IF EXISTS `subjects`;
CREATE TABLE `subjects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of subjects
-- ----------------------------

-- ----------------------------
-- Table structure for `universities`
-- ----------------------------
DROP TABLE IF EXISTS `universities`;
CREATE TABLE `universities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `description` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of universities
-- ----------------------------

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  `name` varchar(30) DEFAULT NULL,
  `college` varchar(50) DEFAULT NULL,
  `year` varchar(4) DEFAULT NULL,
  `mail` varchar(255) DEFAULT NULL,
  `mobile` varchar(11) DEFAULT NULL,
  `pic` varchar(255) DEFAULT 'avatar/default.png',
  `address_1` varchar(255) DEFAULT NULL,
  `address_2` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `is_student` tinyint(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of users
-- ----------------------------
