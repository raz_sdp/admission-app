<?php echo $this->element('menu'); ?>
<div class="onlineExams form">
<?php echo $this->Form->create('OnlineExam'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Online Exam'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('starting_at');
		echo $this->Form->input('time');
		echo $this->Form->input('no_ques');
		echo $this->Form->input('negate');
		echo $this->Form->input('marks');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('OnlineExam.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('OnlineExam.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Online Exams'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Online Exam Histories'), array('controller' => 'online_exam_histories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Online Exam History'), array('controller' => 'online_exam_histories', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Questions'), array('controller' => 'questions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Question'), array('controller' => 'questions', 'action' => 'add')); ?> </li>
	</ul>
</div>
