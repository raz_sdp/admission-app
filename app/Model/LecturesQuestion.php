<?php
App::uses('AppModel', 'Model');
/**
 * LecturesQuestion Model
 *
 * @property Lecture $Lecture
 * @property Question $Question
 */
class LecturesQuestion extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Lecture' => array(
			'className' => 'Lecture',
			'foreignKey' => 'lecture_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Question' => array(
			'className' => 'Question',
			'foreignKey' => 'question_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
