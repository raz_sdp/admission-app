<?php
App::uses('OnlineExamsController', 'Controller');

/**
 * OnlineExamsController Test Case
 *
 */
class OnlineExamsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.online_exam',
		'app.online_exam_history',
		'app.user',
		'app.calendar_entry',
		'app.comment',
		'app.post',
		'app.subject',
		'app.question',
		'app.admission_type',
		'app.ranking',
		'app.admission_types_user',
		'app.university',
		'app.lecture',
		'app.lesson',
		'app.section',
		'app.completance_history',
		'app.lectures_question',
		'app.label',
		'app.labels_post',
		'app.report',
		'app.badge',
		'app.badges_user'
	);

/**
 * testAdminIndex method
 *
 * @return void
 */
	public function testAdminIndex() {
		$this->markTestIncomplete('testAdminIndex not implemented.');
	}

/**
 * testAdminView method
 *
 * @return void
 */
	public function testAdminView() {
		$this->markTestIncomplete('testAdminView not implemented.');
	}

/**
 * testAdminAdd method
 *
 * @return void
 */
	public function testAdminAdd() {
		$this->markTestIncomplete('testAdminAdd not implemented.');
	}

/**
 * testAdminEdit method
 *
 * @return void
 */
	public function testAdminEdit() {
		$this->markTestIncomplete('testAdminEdit not implemented.');
	}

/**
 * testAdminDelete method
 *
 * @return void
 */
	public function testAdminDelete() {
		$this->markTestIncomplete('testAdminDelete not implemented.');
	}

}
