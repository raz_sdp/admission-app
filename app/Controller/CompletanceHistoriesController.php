<?php
App::uses('AppController', 'Controller');
/**
 * CompletanceHistories Controller
 *
 * @property CompletanceHistory $CompletanceHistory
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class CompletanceHistoriesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->CompletanceHistory->recursive = 0;
		$this->set('completanceHistories', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->CompletanceHistory->exists($id)) {
			throw new NotFoundException(__('Invalid completance history'));
		}
		$options = array('conditions' => array('CompletanceHistory.' . $this->CompletanceHistory->primaryKey => $id));
		$this->set('completanceHistory', $this->CompletanceHistory->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->CompletanceHistory->create();
			if ($this->CompletanceHistory->save($this->request->data)) {
				$this->Session->setFlash(__('The completance history has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The completance history could not be saved. Please, try again.'));
			}
		}
		$users = $this->CompletanceHistory->User->find('list');
		$lectures = $this->CompletanceHistory->Lecture->find('list');
		$this->set(compact('users', 'lectures'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->CompletanceHistory->exists($id)) {
			throw new NotFoundException(__('Invalid completance history'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->CompletanceHistory->save($this->request->data)) {
				$this->Session->setFlash(__('The completance history has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The completance history could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('CompletanceHistory.' . $this->CompletanceHistory->primaryKey => $id));
			$this->request->data = $this->CompletanceHistory->find('first', $options);
		}
		$users = $this->CompletanceHistory->User->find('list');
		$lectures = $this->CompletanceHistory->Lecture->find('list');
		$this->set(compact('users', 'lectures'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->CompletanceHistory->id = $id;
		if (!$this->CompletanceHistory->exists()) {
			throw new NotFoundException(__('Invalid completance history'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->CompletanceHistory->delete()) {
			$this->Session->setFlash(__('The completance history has been deleted.'));
		} else {
			$this->Session->setFlash(__('The completance history could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
