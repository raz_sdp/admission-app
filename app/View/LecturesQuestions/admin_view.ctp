<div class="lecturesQuestions view">
<h2><?php echo __('Lectures Question'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($lecturesQuestion['LecturesQuestion']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Lecture'); ?></dt>
		<dd>
			<?php echo $this->Html->link($lecturesQuestion['Lecture']['name'], array('controller' => 'lectures', 'action' => 'view', $lecturesQuestion['Lecture']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Question'); ?></dt>
		<dd>
			<?php echo $this->Html->link($lecturesQuestion['Question']['id'], array('controller' => 'questions', 'action' => 'view', $lecturesQuestion['Question']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Example'); ?></dt>
		<dd>
			<?php echo h($lecturesQuestion['LecturesQuestion']['example']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($lecturesQuestion['LecturesQuestion']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($lecturesQuestion['LecturesQuestion']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Lectures Question'), array('action' => 'edit', $lecturesQuestion['LecturesQuestion']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Lectures Question'), array('action' => 'delete', $lecturesQuestion['LecturesQuestion']['id']), array(), __('Are you sure you want to delete # %s?', $lecturesQuestion['LecturesQuestion']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Lectures Questions'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Lectures Question'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Lectures'), array('controller' => 'lectures', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Lecture'), array('controller' => 'lectures', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Questions'), array('controller' => 'questions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Question'), array('controller' => 'questions', 'action' => 'add')); ?> </li>
	</ul>
</div>
