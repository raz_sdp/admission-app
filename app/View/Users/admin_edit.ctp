<div class="users form">
<?php echo $this->Form->create('User'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit User'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('username');
		echo $this->Form->input('password');
		echo $this->Form->input('name');
		echo $this->Form->input('college');
		echo $this->Form->input('year');
		echo $this->Form->input('mail');
		echo $this->Form->input('mobile');
		echo $this->Form->input('pic');
		echo $this->Form->input('address_1');
		echo $this->Form->input('address_2');
		echo $this->Form->input('city');
		echo $this->Form->input('is_student');
		echo $this->Form->input('AdmissionType');
		echo $this->Form->input('Badge');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('User.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('User.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Calendar Entries'), array('controller' => 'calendar_entries', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Calendar Entry'), array('controller' => 'calendar_entries', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Comments'), array('controller' => 'comments', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Comment'), array('controller' => 'comments', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Completance Histories'), array('controller' => 'completance_histories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Completance History'), array('controller' => 'completance_histories', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Online Exam Histories'), array('controller' => 'online_exam_histories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Online Exam History'), array('controller' => 'online_exam_histories', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Posts'), array('controller' => 'posts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Post'), array('controller' => 'posts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Rankings'), array('controller' => 'rankings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ranking'), array('controller' => 'rankings', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Reports'), array('controller' => 'reports', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Report'), array('controller' => 'reports', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Admission Types'), array('controller' => 'admission_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Admission Type'), array('controller' => 'admission_types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Badges'), array('controller' => 'badges', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Badge'), array('controller' => 'badges', 'action' => 'add')); ?> </li>
	</ul>
</div>
