<div class="onlineExams view">
<h2><?php echo __('Online Exam'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($onlineExam['OnlineExam']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($onlineExam['OnlineExam']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Starting At'); ?></dt>
		<dd>
			<?php echo h($onlineExam['OnlineExam']['starting_at']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Time'); ?></dt>
		<dd>
			<?php echo h($onlineExam['OnlineExam']['time']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('No Ques'); ?></dt>
		<dd>
			<?php echo h($onlineExam['OnlineExam']['no_ques']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Negate'); ?></dt>
		<dd>
			<?php echo h($onlineExam['OnlineExam']['negate']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Marks'); ?></dt>
		<dd>
			<?php echo h($onlineExam['OnlineExam']['marks']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($onlineExam['OnlineExam']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($onlineExam['OnlineExam']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Online Exam'), array('action' => 'edit', $onlineExam['OnlineExam']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Online Exam'), array('action' => 'delete', $onlineExam['OnlineExam']['id']), array(), __('Are you sure you want to delete # %s?', $onlineExam['OnlineExam']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Online Exams'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Online Exam'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Online Exam Histories'), array('controller' => 'online_exam_histories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Online Exam History'), array('controller' => 'online_exam_histories', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Questions'), array('controller' => 'questions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Question'), array('controller' => 'questions', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Online Exam Histories'); ?></h3>
	<?php if (!empty($onlineExam['OnlineExamHistory'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Online Exam Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Mark'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($onlineExam['OnlineExamHistory'] as $onlineExamHistory): ?>
		<tr>
			<td><?php echo $onlineExamHistory['id']; ?></td>
			<td><?php echo $onlineExamHistory['online_exam_id']; ?></td>
			<td><?php echo $onlineExamHistory['user_id']; ?></td>
			<td><?php echo $onlineExamHistory['mark']; ?></td>
			<td><?php echo $onlineExamHistory['created']; ?></td>
			<td><?php echo $onlineExamHistory['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'online_exam_histories', 'action' => 'view', $onlineExamHistory['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'online_exam_histories', 'action' => 'edit', $onlineExamHistory['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'online_exam_histories', 'action' => 'delete', $onlineExamHistory['id']), array(), __('Are you sure you want to delete # %s?', $onlineExamHistory['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Online Exam History'), array('controller' => 'online_exam_histories', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Questions'); ?></h3>
	<?php if (!empty($onlineExam['Question'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Subject Id'); ?></th>
		<th><?php echo __('Admission Type Id'); ?></th>
		<th><?php echo __('Question'); ?></th>
		<th><?php echo __('Ans A'); ?></th>
		<th><?php echo __('Ans B'); ?></th>
		<th><?php echo __('Ans C'); ?></th>
		<th><?php echo __('Ans D'); ?></th>
		<th><?php echo __('Ans E'); ?></th>
		<th><?php echo __('Cor Ans'); ?></th>
		<th><?php echo __('Explanation'); ?></th>
		<th><?php echo __('University Id'); ?></th>
		<th><?php echo __('Year'); ?></th>
		<th><?php echo __('Online Exam Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($onlineExam['Question'] as $question): ?>
		<tr>
			<td><?php echo $question['id']; ?></td>
			<td><?php echo $question['subject_id']; ?></td>
			<td><?php echo $question['admission_type_id']; ?></td>
			<td><?php echo $question['question']; ?></td>
			<td><?php echo $question['ans_a']; ?></td>
			<td><?php echo $question['ans_b']; ?></td>
			<td><?php echo $question['ans_c']; ?></td>
			<td><?php echo $question['ans_d']; ?></td>
			<td><?php echo $question['ans_e']; ?></td>
			<td><?php echo $question['cor_ans']; ?></td>
			<td><?php echo $question['explanation']; ?></td>
			<td><?php echo $question['university_id']; ?></td>
			<td><?php echo $question['year']; ?></td>
			<td><?php echo $question['online_exam_id']; ?></td>
			<td><?php echo $question['created']; ?></td>
			<td><?php echo $question['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'questions', 'action' => 'view', $question['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'questions', 'action' => 'edit', $question['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'questions', 'action' => 'delete', $question['id']), array(), __('Are you sure you want to delete # %s?', $question['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Question'), array('controller' => 'questions', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
