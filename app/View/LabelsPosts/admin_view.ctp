<div class="labelsPosts view">
<h2><?php echo __('Labels Post'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($labelsPost['LabelsPost']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Post'); ?></dt>
		<dd>
			<?php echo $this->Html->link($labelsPost['Post']['title'], array('controller' => 'posts', 'action' => 'view', $labelsPost['Post']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Label'); ?></dt>
		<dd>
			<?php echo $this->Html->link($labelsPost['Label']['name'], array('controller' => 'labels', 'action' => 'view', $labelsPost['Label']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($labelsPost['LabelsPost']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($labelsPost['LabelsPost']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Labels Post'), array('action' => 'edit', $labelsPost['LabelsPost']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Labels Post'), array('action' => 'delete', $labelsPost['LabelsPost']['id']), array(), __('Are you sure you want to delete # %s?', $labelsPost['LabelsPost']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Labels Posts'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Labels Post'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Posts'), array('controller' => 'posts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Post'), array('controller' => 'posts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Labels'), array('controller' => 'labels', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Label'), array('controller' => 'labels', 'action' => 'add')); ?> </li>
	</ul>
</div>
