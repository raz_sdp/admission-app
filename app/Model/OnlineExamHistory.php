<?php
App::uses('AppModel', 'Model');
/**
 * OnlineExamHistory Model
 *
 * @property OnlineExam $OnlineExam
 * @property User $User
 */
class OnlineExamHistory extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'online_exam_history';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'OnlineExam' => array(
			'className' => 'OnlineExam',
			'foreignKey' => 'online_exam_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
