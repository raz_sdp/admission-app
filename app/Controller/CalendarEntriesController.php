<?php
App::uses('AppController', 'Controller');
/**
 * CalendarEntries Controller
 *
 * @property CalendarEntry $CalendarEntry
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class CalendarEntriesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->CalendarEntry->recursive = 0;
		$this->set('calendarEntries', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->CalendarEntry->exists($id)) {
			throw new NotFoundException(__('Invalid calendar entry'));
		}
		$options = array('conditions' => array('CalendarEntry.' . $this->CalendarEntry->primaryKey => $id));
		$this->set('calendarEntry', $this->CalendarEntry->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->CalendarEntry->create();
			if ($this->CalendarEntry->save($this->request->data)) {
				$this->Session->setFlash(__('The calendar entry has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The calendar entry could not be saved. Please, try again.'));
			}
		}
		$users = $this->CalendarEntry->User->find('list');
		$this->set(compact('users'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->CalendarEntry->exists($id)) {
			throw new NotFoundException(__('Invalid calendar entry'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->CalendarEntry->save($this->request->data)) {
				$this->Session->setFlash(__('The calendar entry has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The calendar entry could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('CalendarEntry.' . $this->CalendarEntry->primaryKey => $id));
			$this->request->data = $this->CalendarEntry->find('first', $options);
		}
		$users = $this->CalendarEntry->User->find('list');
		$this->set(compact('users'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->CalendarEntry->id = $id;
		if (!$this->CalendarEntry->exists()) {
			throw new NotFoundException(__('Invalid calendar entry'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->CalendarEntry->delete()) {
			$this->Session->setFlash(__('The calendar entry has been deleted.'));
		} else {
			$this->Session->setFlash(__('The calendar entry could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
